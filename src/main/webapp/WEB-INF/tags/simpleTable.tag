<%@tag description="Page wrapper" pageEncoding="UTF-8"%>
<%@attribute name="title"%>
<%@attribute name="table_body" fragment="true" %>
<%@attribute name="table_head" fragment="true" %>

<div class="panel panel-default">
    <div class="panel-heading">
        ${title}
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped datatable">
                <thead>
                <tr>
                    <jsp:invoke fragment="table_head"/>
                </tr>
                </thead>
                <tbody>
                 <jsp:invoke fragment="table_body"/>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.panel-body -->
</div>