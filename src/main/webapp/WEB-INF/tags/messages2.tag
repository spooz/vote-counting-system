<%@tag description="Messages2" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" ng-click="loadAccessibleUserList()">
        <i class="fa fa-plus fa-fw"></i>  <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-messages">
        <li><input type="text" placeholder="Search..." ng-model="searchAccessableUserList.name" id="searchAccessableUserList"/></li>
        <li class="divider"></li>
        <li ng-repeat="accessableUser in accessableUsers | filter:searchAccessableUserList" ng-click="openConversation(accessableUser.id, accessableUser.name)">
            <a href="#" id="{{'newChat_'+ accessableUser.name}}">
                    <span class="glyphicon glyphicon-user">
                    </span>
                {{' ' + accessableUser.name}}
            </a>
    </ul>
</li>




