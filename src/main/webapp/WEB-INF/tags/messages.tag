<%@tag description="Messages" pageEncoding="UTF-8"%>
<%@tag import="voting.domain.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="user" required="true" type="voting.domain.User"%>

<div class="container">
    <div ng-repeat="conversation in conversations" ng-init="index = $index" class="row chat-window col-xs-5 col-md-3" id="{{'chat_window_' + conversation.intercutour}}" style="{{'margin-left:10px;left:' + (100 + 300 * index) + 'px'}}" >
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading top-bar">
                    <div class="col-md-8 col-xs-8">
                        <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> {{conversation.intercutour}}</h3>
                    </div>
                    <div class="col-md-4 col-xs-4" style="text-align: right;">
                        <a href="#"><span id="{{'minim_chat_window_'+ conversation.intercutour}}" class="glyphicon glyphicon-minus icon_minim"></span></a>
                        <a href="#" ng-click="closeConversation(conversation.intercutour)" ><span class="glyphicon glyphicon-remove icon_close" data-id="{{'chat_window_' + conversation.intercutour}}"></span></a>
                    </div>
                </div>
                <div class="panel-body msg_container_base">
                    <div ng-repeat="message in conversation.messages" ng-class="{'row msg_container base_sent ': message.sent, 'row msg_container base_receive': !message.sent}">
                        <div ng-if="!message.sent" class="col-md-2 col-xs-2 avatar">
                            <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                        </div>
                        <div class="col-md-10 col-xs-10">
                            <div ng-class="{'messages msg_sent': message.sent, 'messages msg_receive': !message.sent}">
                                <p>{{message.message}}</p>
                                <time datetime="{{message.date}}">{{message.date | date:'medium'}}</time>
                            </div>
                        </div>
                        <div ng-if="message.sent" class="col-md-2 col-xs-2 avatar">
                            <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <form ng-submit="addMessage(conversation.newmessage, conversation.intercutour)" autocomplete="off">
                            <div style="float:left">
                                <input id="{{'btn-input_' + conversation.intercutour}}" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..." ng-model="conversation.newmessage"/>
                            </div>
                            <div style="float:left">
                                <span class="input-group-btn">
                                <input type="submit" class="btn btn-primary btn-sm" id="{{'btn-chat_' + conversation.intercutour}}" value="Send" />
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>