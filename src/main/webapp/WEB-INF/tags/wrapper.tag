<%@tag description="Page wrapper" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="title"%>
<%@attribute name="page_body" fragment="true" %>

<!DOCTYPE html>
<html lang="en" ng-app="chatApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>



    <!-- Bootstrap Core CSS -->
    <link href="/resources/sb-admin/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/resources/sb-admin/bower_components/metisMenu/dist/metisMenu.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/resources/sb-admin/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/resources/sb-admin/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/resources/css/custom.css" rel="stylesheet">
    <link href="/resources/css/messages.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="/resources/sb-admin/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/resources/sb-admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <%--X-editable libraries--%>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.min.css" rel="stylesheet"/>


    <![endif]-->
    <title>${title}</title>
</head>
<body>
    <div ng-controller="ChatController">
        <t:menu user="${currentUser.user}"></t:menu>
        <t:messages user="${currentUser.user}"></t:messages>
    </div>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">${title}</h1>
            </div>
        </div>
        <jsp:invoke fragment="page_body"/>
    </div>





<!-- Metis Menu Plugin JavaScript -->
<script src="/resources/sb-admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="/resources/sb-admin/bower_components/raphael/raphael-min.js"></script>
<script src="/resources/sb-admin/bower_components/morrisjs/morris.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/resources/sb-admin/dist/js/sb-admin-2.js"></script>

<!-- AngularJS -->
<script src="/resources/sb-admin/bower_components/angular/angular.min.js"></script>

<!-- SockJS -->
<script src="/resources/sb-admin/bower_components/sockjs-client/dist/sockjs-1.1.0.min.js"></script>

<!-- StompJS -->
<script src="/resources/sb-admin/bower_components/stomp-websocket/stomp.min.js"></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>

<script src="/resources/js/messages.js"></script>
<script src="/resources/js/messages-services.js"></script>
<script src="/resources/js/messages-controllers.js"></script>
<script src="/resources/js/messages-view.js"></script>
    <script src="/resources/js/scripts.js"></script>

<script>

    $(".validateForm").validate();
</script>
</body>
