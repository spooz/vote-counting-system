<%@tag description="Menu" pageEncoding="UTF-8"%>
<%@tag import="voting.domain.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="user" required="true" type="voting.domain.User"%>

<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" ng-click="getNotifications()">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Voting system</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <t:messages2></t:messages2>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="{{messageNotifications.length == 0 ? 'fa fa-envelope-o fa-fw' : 'fa fa-envelope fa-fw'}}"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                <li ng-repeat="notification in messageNotifications" ng-click="openConversation(notification.senderId, notification.senderName)">
                    <a href="#">
                        <div>
                            <strong>{{ notification.senderName }}</strong>
                                    <span class="pull-right text-muted">
                                        <em><time datetime="{{notification.date}}">{{notification.date | date:'medium'}}</time></em>
                                    </span>
                        </div>
                        <div>{{ notification.message }}</div>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-messages -->
        </li>


        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                ${user.login}
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>

            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="/user/${user.id}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li class="divider"></li>
                <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/"><i class="fa fa-dashboard fa-fw"></i> Home</a>
                </li>
                <c:if test="${currentUser.role != 'ADMIN'}">
                    <li>
                        <a href="/comission/${user.comission.id}">My comission</a>
                    </li>
                </c:if>
                <c:if test="${currentUser.role == 'PKW'}">
                    <li>
                        <a href="/election/create">Create election</a>
                    </li>
                    <li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Committees<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/committee/create">Create committee</a>
                                </li>
                                <li>
                                    <a href="/committees/">All committees</a>
                                </li>
                            </ul>
                        </li>
                        <!-- /.nav-second-level -->
                    </li>
                </c:if>
                <c:if test="${currentUser.role != 'OBWKW'}">
                    <li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/user/create">Create user</a>
                                </li>
                                <li>
                                    <a href="/users/">All users</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-graduation-cap fa-fw"></i> Comission<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${currentUser.role == 'ADMIN'}">
                            <li>
                                <a href="/comission/create">Create comission</a>
                            </li>
                            </c:if>
                            <li>
                                <a href="/comissions/">All comissions</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </c:if>
                <c:if test="${currentUser.role != 'ADMIN'}">
                    <li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Candidates lists <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <c:if test="${currentUser.role == 'PKW' || currentUser.role == 'OKW'}">
                                    <li>
                                        <a href="/votinglists/add">Create list</a>
                                    </li>
                                </c:if>
                                <li>
                                    <a href="/votinglists">All lists</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </li>
                </c:if>
                    <li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Votes <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <c:if test="${currentUser.role == 'OBWKW'}">
                            <li>
                                <a href="/votes/candidates">Insert scores</a>
                            </li>
                        </c:if>

                            <c:if test="${currentUser.role == 'OKW' or currentUser.role == 'PKW'}">
                                <li>
                                    <a href="/votes/candidates/show">Show scores</a>
                                </li>
                            </c:if>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>