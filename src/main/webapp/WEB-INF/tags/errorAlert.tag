<%@tag description="errorAlert" pageEncoding="UTF-8"%>
<%@attribute name="alert_content" fragment="true" %>

<div class="alert alert-danger">
  <jsp:invoke fragment="alert_content" />
</div>