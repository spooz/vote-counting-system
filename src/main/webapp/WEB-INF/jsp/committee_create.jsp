<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Create committee">
    <jsp:attribute name="page_body">
        <div class="col-lg-6">
            <form:form commandName="form" role="form" name="form" action="/committee/create" method="post">

                <spring:hasBindErrors name="form">
                    <t:errorAlert>
                        <jsp:attribute name="alert_content">
                            <c:forEach var="error" items="${errors.globalErrors}">
                                ${error.defaultMessage}
                            </c:forEach>
                         </jsp:attribute>
                    </t:errorAlert>
                </spring:hasBindErrors>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="form-group">
                    <form:label path="fullName" for="fullName" cssErrorClass="error-label">Full Name</form:label>
                    <form:input cssErrorClass="form-control error-input" path="fullName" class="form-control" type="text" name="fullName" id="fullName"  autofocus="true"/>
                    <form:errors path="fullName" cssClass="error-label"></form:errors>
                </div>
                <div class="form-group">
                     <form:label path="shortName" for="shortName" cssErrorClass="error-label">Short Name</form:label>
                     <form:input cssErrorClass="form-control error-input" path="shortName" class="form-control" type="text" name="shortName" id="shortName"  autofocus="true"/>
                     <form:errors path="shortName" cssClass="error-label"></form:errors>
                </div>
                <button class="btn btn-success" type="submit">Save</button>
            </form:form>
        </div>

    </jsp:attribute>

</t:wrapper>