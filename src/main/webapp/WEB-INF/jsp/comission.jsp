<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Comission details">
    <jsp:attribute name="page_body">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">ID: ${comission.id}</h3>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>Code:</td>
                                <td>${comission.code}</td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td><a href="#" class="comUpdate" id="address" data-type="text" data-pk="1" data-url="/comission/${comission.id}" data-title="Enter username">${comission.address}</a></td>
                            </tr>
                            <tr>
                                <td>Type:</td>
                                <td>${comission.comissionType}</td>
                            </tr>
                            <tr>
                                <td>Parent id:</td>
                                <td>${comission.parentComission.id}</td>
                            </tr>
                            <c:if test="${comission.comissionType == 'OKW' && currentUser.role == 'PKW'}">
                                <tr>
                                    <td>Voting lists:</td>
                                    <td><a href="/votinglists/ <c:out value='${comission.id}'/>">Lists</a></td>
                                </tr>
                            </c:if>
                            </tbody>
                        </table>

                        <button id="${comission.id}" class="btn btn-danger delCom" type="submit">Delete</button>
                        <a href="/votinglists/${comission.id}" class="btn btn-primary">Voting lists</a>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
            </div>

        </div>

    </jsp:attribute>

</t:wrapper>