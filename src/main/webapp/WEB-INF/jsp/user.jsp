<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="User details">
    <jsp:attribute name="page_body">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">ID: ${user.id}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>Login:</td>
                                <td>${user.login}</td>
                            </tr>
                            <tr>
                                <td>Role:</td>
                                <td>Role: ${user.role}</td>
                            </tr>


                            </tbody>
                        </table>

                        <c:if test="${currentUser.id != user.id}">
                            <button id="${user.id}" class="btn btn-danger delUsr" type="submit">Delete</button>
                        </c:if>

                    </div>
                </div>
            </div>
            <div class="panel-footer">
            </div>

        </div>

    </jsp:attribute>

</t:wrapper>


