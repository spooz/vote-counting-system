<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Create election">
    <jsp:attribute name="page_body">
        <div class="col-lg-6">
            <c:if test="${error}">
                <div class=" alert alert-danger">The election dates have errors!</div>

            </c:if>
            <form:form  modelAttribute="election" action="/election/create" method="post">


                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="form-group">
                    <form:label path="name" for="name" >Name:</form:label>
                    <form:input class="form-control" required="required" name="name" path="name" />
                </div>

                <div class="form-group">
                    <form:label path="electionType" for="electionType" cssErrorClass="error-label">Role</form:label>
                    <form:select cssErrorClass="form-control error-input" path="electionType" class="form-control" name="electionType" id="electionType" required="true">
                        <option value="PRESIDENTIAL">PRESIDENTIAL</option>
                        <option value="DISTRICT">DISTRICT</option>
                    </form:select>
                    <form:errors path="electionType" cssClass="error-label"></form:errors>
                </div>

                <div class="form-group">
                    <form:label path="startDate" for="startDate" cssErrorClass="error-label">Start Date</form:label>
                    <form:input path="startDate" class="form-control" name="startDate" type="text" placeholder="click to choose start date" id="startDate" required="true"/>
                    <form:errors path="startDate" cssClass="error-label"></form:errors>
                </div>

                <div class="form-group">
                    <form:label path="endDate" for="endDate" cssErrorClass="error-label">End Date</form:label>
                    <form:input path="endDate" class="form-control" name="endDate" type="text" placeholder="click to choose end date" id="endDate" required="true"/>
                    <form:errors path="endDate" cssClass="error-label"></form:errors>
                </div>

                <button class="btn btn-success" type="submit">Save</button>
            </form:form>
            <script type="text/javascript">
                // When the document is ready
                $(document).ready(function () {

                    $('#startDate').datepicker({
                        format: "dd/mm/yyyy"
                    });
                    $('#endDate').datepicker({
                        format: "dd/mm/yyyy"
                    });

                });
            </script>
        </div>

    </jsp:attribute>

</t:wrapper>


