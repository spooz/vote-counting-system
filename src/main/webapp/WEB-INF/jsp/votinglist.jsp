<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Voting list details">
    <jsp:attribute name="page_body">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">ID: ${votingList.id}</h3>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>List number:</td>
                                <td>${votingList.listNumber}</td>
                            </tr>
                            <tr>
                                <td>Commission:</td>
                                <td>${votingList.comission.id}</td>
                            </tr>
                            <c:if test="${votingList.committee != null}">
                                <tr>
                                    <td>Committee:</td>
                                    <td>${votingList.committee.fullName}</td>
                                </tr>
                            </c:if>
                            <tr>
                                <td>Candidates:</td>
                                <c:forEach var="candidate" items="${votingList.candidates}">
                                    <tr>
                                        <td></td>
                                        <td><a href="/candidate/<c:out value='${candidate.id}'/>">${candidate.firstName} ${candidate.lastName}</a></td>
                                    </tr>
                                </c:forEach>
                            </tr>

                            </tbody>
                        </table>
                        <a href="/list/${votingList.id}/candidate/add" class="btn btn-success">Add candidate</a>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
            </div>

        </div>

    </jsp:attribute>

</t:wrapper>