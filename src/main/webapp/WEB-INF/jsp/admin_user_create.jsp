<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Create user">
    <jsp:attribute name="page_body">
        <div class="col-lg-6">
            <form:form commandName="form" role="form" name="form" action="/user/create" method="post">


                <spring:hasBindErrors name="form">
                    <t:errorAlert>
                        <jsp:attribute name="alert_content">
                            <c:forEach var="error" items="${errors.globalErrors}">
                                ${error.defaultMessage}
                            </c:forEach>
                         </jsp:attribute>
                    </t:errorAlert>
                </spring:hasBindErrors>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="form-group">
                    <form:label path="login" for="login" cssErrorClass="error-label">Login:</form:label>
                    <form:input cssErrorClass="form-control error-input" path="login" class="form-control" type="text" name="login" id="login" value="${form.login}" autofocus="true"/>
                    <form:errors path="login" cssClass="error-label"></form:errors>
                </div>
                <div class="form-group">
                    <form:label path="password" for="password" cssErrorClass="error-label">Password</form:label>
                    <form:input cssErrorClass="form-control error-input" path="password" class="form-control" type="password" name="password" id="password" required="true"/>
                    <form:errors path="password" cssClass="error-label"></form:errors>
                </div>
                <div class="form-group">
                    <form:label path="passwordRepeated" for="passwordRepeated" cssErrorClass="error-label">Repeat</form:label>
                    <form:input cssErrorClass="form-control error-input" path="passwordRepeated" class="form-control" type="password" name="passwordRepeated" id="passwordRepeated" required="true"/>
                    <form:errors path="passwordRepeated" cssClass="error-label"></form:errors>
                </div>
                <div class="form-group">
                    <form:label path="comission" for="comission" cssErrorClass="error-label">Comission</form:label>
                    <form:select cssErrorClass="form-control error-input" path="comission" class="form-control" name="comission" id="comission">
                        <option selected>null</option>
                        <c:forEach var="comission" items="${comissions}">
                            <option value=${comission.id}>${comission.comissionType} ${comission.code} ${comission.address}</option>
                        </c:forEach>
                    </form:select>
                    <form:errors path="comission" cssClass="error-label"></form:errors>
                </div>
                <button class="btn btn-success" type="submit">Save</button>
            </form:form>
        </div>

    </jsp:attribute>

</t:wrapper>


