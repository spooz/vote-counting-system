<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Candidates - votes">
    <jsp:attribute name="page_body">


        <div style="margin-bottom: 10px;" class="row">
            <a href="/votes/candidates/show/pdf" role="button" class="btn btn-success">Download report</a>
        </div>



        <c:forEach var="view" items="${views}">
            <div style="margin-bottom: 10px;" class="row">
            <c:choose>
                <c:when test="${currentUser.role != 'OKW'}">
                    <t:simpleTable title="Comission: ${view.votingList.comission.code} List: ${view.votingList.listNumber} - ${view.votingList.committee.fullName}">
                        <jsp:attribute name="table_head">
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Current Score</th>
                            <th>Comissions</th>
                        </jsp:attribute>
                        <jsp:attribute name="table_body">
                            <c:forEach var="candidateVotes" items="${view.votesResults}">
                                <tr>
                                    <td>${candidateVotes.candidate.firstName}</td>
                                    <td>${candidateVotes.candidate.lastName}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${candidateVotes.votes == -1}">
                                                No votes uploaded.
                                            </c:when>
                                            <c:otherwise>
                                                ${candidateVotes.votes}
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>${candidateVotes.comissions} / ${view.allComissions}</td>
                                </tr>

                            </c:forEach>
                        </jsp:attribute>
                    </t:simpleTable>
                </c:when>
                <c:otherwise>
                    <t:simpleTable title="List: ${view.votingList.listNumber} - ${view.votingList.committee.fullName}">
                        <jsp:attribute name="table_head">
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Current Score</th>
                            <th>Comissions</th>
                        </jsp:attribute>
                        <jsp:attribute name="table_body">
                            <c:forEach var="candidateVotes" items="${view.votesResults}">
                                <tr>
                                    <td>${candidateVotes.candidate.firstName}</td>
                                    <td>${candidateVotes.candidate.lastName}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${candidateVotes.votes == -1}">
                                                No votes uploaded.
                                            </c:when>
                                            <c:otherwise>
                                                ${candidateVotes.votes}
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>${candidateVotes.comissions} / ${view.allComissions}</td>
                                </tr>

                            </c:forEach>
                        </jsp:attribute>
                    </t:simpleTable>
                </c:otherwise>
            </c:choose>


            </div>


        </c:forEach>

    </jsp:attribute>

</t:wrapper>

