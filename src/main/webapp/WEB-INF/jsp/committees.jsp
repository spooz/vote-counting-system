<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Committees">
    <jsp:attribute name="page_body">
        <div class="row">
            <t:simpleTable title="Committees">
               <jsp:attribute name="table_head">
                   <th>Short name</th>
                   <th>Full name</th>
               </jsp:attribute>
               <jsp:attribute name="table_body">
                   <c:forEach var="committee" items="${committees}">
                       <tr>
                           <td><a href="/committee/<c:out value='${committee.id}'/>">${committee.shortName}</a></td>
                           <td>${committee.fullName}</td>
                       </tr>
                   </c:forEach>
               </jsp:attribute>
            </t:simpleTable>
        </div>
    </jsp:attribute>

</t:wrapper>