<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Candidate details">
    <jsp:attribute name="page_body">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a href="#" class="candidateUpdate" id="firstName" data-type="text" data-pk="1" data-url="/candidate/${candidate.id}" data-title="Enter firstname">${candidate.firstName}</a>
                    <a href="#" class="candidateUpdate" id="lastName" data-type="text" data-pk="1" data-url="/candidate/${candidate.id}" data-title="Enter lastname">${candidate.lastName}</a>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>
                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>Voting List:</td>
                                    <td>${candidate.votingList.listNumber}</td>
                                </tr>
                            </tbody>
                        </table>
                        <c:if test="${accessible}">
                            <button id="${candidate.id}" class="btn btn-danger delCandidate" type="submit">Delete</button>
                        </c:if>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
            </div>

        </div>

    </jsp:attribute>

</t:wrapper>


