<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Create list">
    <jsp:attribute name="page_body">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-body">
            <form:form commandName="form" role="form" name="form" action="/votinglists/add" method="post">


                <spring:hasBindErrors name="form">
                    <t:errorAlert>
                        <jsp:attribute name="alert_content">
                            <c:forEach var="error" items="${errors.globalErrors}">
                                ${error.defaultMessage}
                            </c:forEach>
                         </jsp:attribute>
                    </t:errorAlert>
                </spring:hasBindErrors>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <c:if test="${currentUser.role == 'OKW'}">
                    <div class="form-group">
                        <form:label path="listNumber" for="listNumber">List Number:</form:label>
                        <form:input class="form-control" name="listNumber" path="listNumber" type="number" required="true"/>
                    </div>
                </c:if>

                <div class="form-group">
                    <form:label path="election" for="election" cssErrorClass="error-label">Election</form:label>
                    <form:select cssErrorClass="form-control error-input" path="election" class="form-control" name="election" id="election" required="true">
                        <c:forEach var="election" items="${elections}">
                            <c:if test="${(currentUser.role == 'OKW' && election.electionType.toString() == 'DISTRICT') ||
                                (currentUser.role == 'PKW' && election.electionType.toString() == 'PRESIDENTIAL')}">
                                <option value=${election.id}>${election.electionType} ${election.name}</option>
                            </c:if>
                        </c:forEach>
                    </form:select>
                    <form:errors path="election" cssClass="error-label"></form:errors>
                </div>

                <c:if test="${currentUser.role == 'OKW'}">
                    <div class="form-group">
                        <form:label path="committee" for="committee" cssErrorClass="error-label">Committee</form:label>
                        <form:select cssErrorClass="form-control error-input" path="committee" class="form-control" name="committee" id="committee" required="true">
                            <c:forEach var="committee" items="${committees}">
                                <option value=${committee.id}>${committee.shortName} ${committee.fullName}</option>
                            </c:forEach>
                        </form:select>
                        <form:errors path="committee" cssClass="error-label"></form:errors>
                    </div>
                </c:if>

                <button class="btn btn-success" type="submit">Save</button>
            </form:form>
        </div>
            </div>
        </div>

    </jsp:attribute>

</t:wrapper>


