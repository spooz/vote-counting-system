<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:wrapper title="Home">
    <jsp:attribute name="page_body">
        <c:if test="${error != null}">
            <b>${error}</b>
        </c:if>
        <c:if test="${currentUser.role != 'ADMIN'}">
            <p><h3>My current election: ${currentUser.user.election.name} </h3></p>
            <form method="post" action="/election/change">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="hidden" name="path" value="/"/>
                <div class="form-group">
                    <select name="election" class="form-control">
                        <c:forEach items="${elections}" var="election">
                            <option value="${election.id}">${election.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <button class="btn btn-success" type="submit">Save</button>
            </form>
        </c:if>
    </jsp:attribute>

</t:wrapper>
