<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Candidates - votes">
    <jsp:attribute name="page_body">


        <c:forEach var="view" items="${views}">
            <div style="margin-bottom: 10px;" class="row">
           <t:simpleTable title="${view.votingList.listNumber} - ${view.votingList.committee.fullName}">
               <jsp:attribute name="table_head">
                   <th>First Name</th>
                   <th>Last Name</th>
                   <th>Current Score</th>
               </jsp:attribute>
               <jsp:attribute name="table_body">
                   <c:forEach var="candidateVotes" items="${view.votesResults}">
                       <tr>
                           <td>${candidateVotes.candidate.firstName}</td>
                           <td>${candidateVotes.candidate.lastName}</td>
                           <td>
                                 <c:if test="${candidateVotes.votes == -1}">
                                   <a href="#" previous-value="${candidateVotes.votes}" name="voteAdd${candidateVotes.candidate.id}" class="voteAdd" id="${candidateVotes.candidate.id}" candidateId="${candidateVotes.candidate.id}" data-type="text" data-pk="1" data-url="/votes/add" data-title="Enter new value">--</a>
                               </c:if>
                               <c:if test="${candidateVotes.votes > -1}">
                                   <a href="#" previous-value="${candidateVotes.votes}" name="voteAdd${candidateVotes.candidate.id}" class="voteAdd" id="${candidateVotes.candidate.id}" candidateId="${candidateVotes.candidate.id}" data-type="text" data-pk="1" data-url="/votes/add" data-title="Enter new value">${candidateVotes.votes}</a>
                               </c:if>




                           </td>
                       </tr>

                   </c:forEach>




               </jsp:attribute>
           </t:simpleTable>
             </div>


        </c:forEach>

    </jsp:attribute>

</t:wrapper>
