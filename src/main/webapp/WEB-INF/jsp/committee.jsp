<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Committee details">
    <jsp:attribute name="page_body">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">ID: ${committee.id}</h3>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                            <tbody>
                            <tr>
                                <td>Name:</td>
                                <td>${committee.fullName}</td>
                            </tr>
                            <tr>
                                <td>Shortcut:</td>
                                <td>${committee.shortName}</td>
                            </tr>
                            </tbody>
                        </table>

                        <%--<button id="${committee.id}" class="btn btn-danger delCom" type="submit">Delete</button>--%>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
            </div>

        </div>

    </jsp:attribute>

</t:wrapper>