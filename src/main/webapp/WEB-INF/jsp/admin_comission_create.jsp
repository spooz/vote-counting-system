<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Create comission">
    <jsp:attribute name="page_body">
        <div class="col-lg-6">
            <form:form commandName="form" role="form" name="form" action="/comission/create" method="post">


                <spring:hasBindErrors name="form">
                    <t:errorAlert>
                        <jsp:attribute name="alert_content">
                            <c:forEach var="error" items="${errors.globalErrors}">
                                ${error.defaultMessage}
                            </c:forEach>
                         </jsp:attribute>
                    </t:errorAlert>
                </spring:hasBindErrors>

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="form-group">
                    <form:label path="code" for="code" cssErrorClass="error-label">Code</form:label>
                    <form:input cssErrorClass="form-control error-input" path="code" class="form-control" type="number" name="code" id="code"  autofocus="true"/>
                    <form:errors path="code" cssClass="error-label"></form:errors>
                </div>
                <div class="form-group">
                    <form:label path="address" for="address" cssErrorClass="error-label">Address</form:label>
                    <form:textarea cssErrorClass="form-control error-input" path="address" class="form-control" type="address" name="address" id="address" required="true"/>
                    <form:errors path="address" cssClass="error-label"></form:errors>
                </div>
                <div class="form-group">
                    <form:label path="comissionType" for="comissionType" cssErrorClass="error-label">Role</form:label>
                    <form:select cssErrorClass="form-control error-input" path="comissionType" class="form-control" name="comissionType" id="comissionType" required="true">
                        <option <c:if test="${form.comissionType == 'PKW'}">selected</c:if>>PKW</option>
                        <option <c:if test="${form.comissionType == 'OKW'}">selected</c:if>>OKW</option>
                        <option <c:if test="${form.comissionType == 'ObKW'}">selected</c:if>>ObKW</option>
                    </form:select>
                    <form:errors path="comissionType" cssClass="error-label"></form:errors>
                </div>
                <div class="form-group">
                    <form:label path="parentComission" for="parentComission" cssErrorClass="error-label">Parent Comission</form:label>
                    <form:select cssErrorClass="form-control error-input" path="parentComission" class="form-control" name="parentComission" id="parentComission">
                        <option selected>null</option>
                        <c:forEach var="comission" items="${comissions}">
                            <option value=${comission.id}>${comission.comissionType} ${comission.code} ${comission.address}</option>
                        </c:forEach>
                    </form:select>
                    <form:errors path="parentComission" cssClass="error-label"></form:errors>
                </div>
                <button class="btn btn-success" type="submit">Save</button>
            </form:form>
        </div>

    </jsp:attribute>

</t:wrapper>
