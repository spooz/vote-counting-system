<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Votling lists">

    <jsp:attribute name="page_body">

        <c:if test="${currentUser.role != 'ADMIN'}">
            <p>current election: ${currentUser.user.election.name}</p>
            <form method="post" action="/election/change">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="hidden" name="path" value="/votinglists"/>
                <div class="form-group">
                    <select name="election" class="form-control">
                        <c:forEach items="${elections}" var="election">
                            <option value="${election.id}">${election.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <button class="btn btn-success" type="submit">Save</button>
            </form>
        </c:if>

        <br/>

        <div class="row">
            <t:simpleTable title="Voting lists">
               <jsp:attribute name="table_head">
                   <th>Number</th>
               </jsp:attribute>
               <jsp:attribute name="table_body">
                   <c:forEach var="votinglist" items="${votingLists}">
                       <tr>
                           <td><a href="/votinglist/ <c:out value='${votinglist.id}'/>">${votinglist.listNumber} ${votinglist.committee.fullName}</a></td>
                       </tr>
                   </c:forEach>
               </jsp:attribute>
            </t:simpleTable>
        </div>

    </jsp:attribute>

</t:wrapper>
