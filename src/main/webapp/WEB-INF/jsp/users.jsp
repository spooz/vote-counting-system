<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Users">
    <jsp:attribute name="page_body">
        <div class="row">
           <t:simpleTable title="Users">
               <jsp:attribute name="table_head">
                   <th>Login</th>
                    <th>Role</th>
               </jsp:attribute>
               <jsp:attribute name="table_body">
                   <c:forEach var="user" items="${users}">
                       <tr>
                           <td><a href="/user/ <c:out value='${user.id}'/>">${user.login}</a></td>
                           <td>${user.role}</td>
                       </tr>
                   </c:forEach>
               </jsp:attribute>
           </t:simpleTable>
        </div>
    </jsp:attribute>

</t:wrapper>
