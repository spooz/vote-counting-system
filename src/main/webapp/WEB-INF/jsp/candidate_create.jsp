<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Create candidate">
    <jsp:attribute name="page_body">
        <div class="col-lg-6">
            <form:form  modelAttribute="candidate" action="/list/${votingList.id}/candidate/add" method="post">

                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="form-group">
                    <form:label path="firstName" for="firstName">First Name:</form:label>
                    <form:input class="form-control" name="firstName" path="firstName" required="true" />
                    <form:errors path="firstName" cssClass="error-label"></form:errors>
                </div>

                <div class="form-group">
                    <form:label path="lastName" for="lastName">Last Name:</form:label>
                    <form:input class="form-control" name="lastName" path="lastName" required="true" />
                    <form:errors path="lastName" cssClass="error-label"></form:errors>
                </div>

                <button class="btn btn-success" type="submit">Save</button>
            </form:form>
        </div>

    </jsp:attribute>

</t:wrapper>


