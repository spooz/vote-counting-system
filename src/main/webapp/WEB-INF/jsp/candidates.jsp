<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Candidates">
    <jsp:attribute name="page_body">
        <div class="row">
            <t:simpleTable title="Candidates">
               <jsp:attribute name="table_head">
                   <th>ID</th>
                   <th>First Name</th>
                    <th>Last Name</th>
                    <th>Voting List</th>
               </jsp:attribute>
               <jsp:attribute name="table_body">
                   <c:forEach var="candidate" items="${candidates}">
                       <tr>
                           <td><a href="/candidate/<c:out value='${candidate.id}'/>">${candidate.id}</a></td>
                           <td>${candidate.firstName}</td>
                           <td>${candidate.lastName}</td>
                           <td>${candidate.votingList.listNumber}</td>
                       </tr>
                   </c:forEach>
               </jsp:attribute>
            </t:simpleTable>
        </div>
    </jsp:attribute>

</t:wrapper>
