<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper title="Comissions">
    <jsp:attribute name="page_body">
        <div class="row">
            <t:simpleTable title="Comissions">
               <jsp:attribute name="table_head">
                   <th>Code</th>
                    <th>Address</th>
                    <th>Type</th>
               </jsp:attribute>
               <jsp:attribute name="table_body">
                   <c:forEach var="comission" items="${comissions}">
                       <tr>
                           <td><a href="/comission/ <c:out value='${comission.id}'/>">${comission.code}</a></td>
                           <td>${comission.address}</td>
                           <td>${comission.comissionType}</td>
                       </tr>
                   </c:forEach>
               </jsp:attribute>
            </t:simpleTable>
        </div>
    </jsp:attribute>

</t:wrapper>
