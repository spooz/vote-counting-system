<%--
  Created by IntelliJ IDEA.
  User: Bartosz
  Date: 2015-12-13
  Time: 14:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link href="/resources/sb-admin/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
  <title>403</title>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="error-template">
            <h1>403 - Forbidden</h1>

            <div class="error-actions well well-lg">
              <p>Sorry, you don't have access to the resource.</p>
              <a title="Return to the previous page" href="javascript:history.go(-1);" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-circle-arrow-left"></span>
                Previous Page</a>
              <a title="Home" href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-dashboard"></span> Application Home </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
