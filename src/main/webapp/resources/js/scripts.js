/**
 * Created by KOZAK on 2016-06-15.
 */

$.fn.editable.defaults.mode = 'popup';
$.fn.editable.defaults.ajaxOptions = {
    type: "PUT",
    headers: {"X-CSRF-TOKEN": $("meta[name='_csrf']").attr("content")}
};

$(document).ready(function() {
    $('.comUpdate').editable();
});

$(document).ready(function() {
    $('.candidateUpdate').editable();
});

$(document).ready(function() {
    $('.voteAdd').editable({
        params: function(params) {
            params.candidateId = $(this).attr("candidateId");
            params.previousValue = $(this).attr("previous-value");
            return params;
        },
        ajaxOptions: {
            type: 'post',
            dataType: 'json',
            headers: {"X-CSRF-TOKEN": $("meta[name='_csrf']").attr("content")}
        },
        mode: 'popup',
        success: function (res) {
            var postLink = document.getElementsByName("voteAdd" + res.candidateId)[0];
            postLink.setAttribute("previous-value", res.previousValue);
        },
        error: function (err) {
            alert("Coś, coś się popsuło :(\nEhh...")
        }
    });
});

$(".delUsr").click(function(){
    var id = $(this).attr("id");
    var token = $("meta[name='_csrf']").attr("content");
    $.ajax({
        url: '/user/' + id,
        type: 'DELETE',
        headers: {"X-CSRF-TOKEN": token},
        success: function () {
            window.location.replace("/users")
        }
    });
});

$(".delCom").click(function(){
    var id = $(this).attr("id");
    var token = $("meta[name='_csrf']").attr("content");
    $.ajax({
        url: '/comission/' + id,
        type: 'DELETE',
        headers: {"X-CSRF-TOKEN": token},
        success: function () {
            window.location.replace("/comissions")
        }
    });
});

$(".delCandidate").click(function(){
    var id = $(this).attr("id");
    var token = $("meta[name='_csrf']").attr("content");
    $.ajax({
        url: '/candidate/' + id,
        type: 'DELETE',
        headers: {"X-CSRF-TOKEN": token},
        success: function (id) {
            window.location.replace("/votinglist/ " + id)
        }
    });
});

$(document).ready(function() {
    $(".datatable").DataTable();
});