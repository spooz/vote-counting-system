
angular.module("chatApp.services").service("ChatService", function($q, $timeout) {

        var service = {}, listener = $q.defer(), socket = {
            client: null,
            stomp: null
        }, messageIds = [];

        service.RECONNECT_TIMEOUT = 30000;
        service.SOCKET_URL = "/chat";
        service.CHAT_TOPIC = "/user/topic/messages";
        service.CHAT_BROKER = "/app/chat";
        //service.CHAT_BROKER = ['/user/', '/chat'];

        service.receive = function() {
            return listener.promise;
        };

        service.send = function(message, name) {
            var id = Math.floor(Math.random() * 1000000);
            socket.stomp.send((service.CHAT_BROKER/*[0] + name + service.CHAT_BROKER[1]*/), {
                priority: 9
            }, JSON.stringify({
                message: message,
                receiverName: name
            }));
        };

        var reconnect = function() {
            $timeout(function() {
                initialize();
            }, this.RECONNECT_TIMEOUT);
        };

        var getMessage = function(data) {
            return JSON.parse(data);
        };

        var startListener = function() {
            socket.stomp.subscribe(service.CHAT_TOPIC, function(data) {
                listener.notify(getMessage(data.body));
            });
        };

        var initialize = function() {
            socket.client = new SockJS(service.SOCKET_URL);
            socket.stomp = Stomp.over(socket.client);
            socket.stomp.connect({}, startListener);
            socket.stomp.onclose = reconnect;
        };

        initialize();
        return service;
    })


    .service("AccessibleUsersService", function($q, $timeout) {

        var service = {}, listener = $q.defer(), socket = {
            client: null,
            stomp: null
        }, messageIds = [];

        service.RECONNECT_TIMEOUT = 30000;
        service.SOCKET_URL = "/accessibleusers";
        service.CHAT_TOPIC = "/user/topic/accessibleusers";
        service.CHAT_BROKER = "/app/accessibleusers";

        service.receive = function() {
            return listener.promise;
        };

        service.send = function(message) {
            socket.stomp.send(service.CHAT_BROKER, {
                priority: 9
            }, JSON.stringify(""));
        };

        var reconnect = function() {
            $timeout(function() {
                initialize();
            }, this.RECONNECT_TIMEOUT);
        };

        var getMessage = function(data) {
            return JSON.parse(data);
        };

        var startListener = function() {
            socket.stomp.subscribe(service.CHAT_TOPIC, function(data) {
                listener.notify(getMessage(data.body));
            });
        };

        var initialize = function() {
            socket.client = new SockJS(service.SOCKET_URL);
            socket.stomp = Stomp.over(socket.client);
            socket.stomp.connect({}, startListener);
            socket.stomp.onclose = reconnect;
        };

        initialize();
        return service;
    })


    .service("OpenConversationService", function($q, $timeout) {

        var service = {}, listener = $q.defer(), socket = {
            client: null,
            stomp: null
        }, messageIds = [];

        service.RECONNECT_TIMEOUT = 30000;
        service.SOCKET_URL = "/openConversation";
        service.CHAT_TOPIC = "/user/topic/openConversation";
        service.CHAT_BROKER = "/app/openConversation";

        service.receive = function() {
            return listener.promise;
        };

        service.send = function(message) {
            socket.stomp.send(service.CHAT_BROKER, {
                priority: 9
            }, JSON.stringify(message));
        };

        var reconnect = function() {
            $timeout(function() {
                initialize();
            }, this.RECONNECT_TIMEOUT);
        };

        var getMessage = function(data) {
            return JSON.parse(data);
        };

        var startListener = function() {
            socket.stomp.subscribe(service.CHAT_TOPIC, function(data) {
                listener.notify(getMessage(data.body));
            });
        };

        var initialize = function() {
            socket.client = new SockJS(service.SOCKET_URL);
            socket.stomp = Stomp.over(socket.client);
            socket.stomp.connect({}, startListener);
            socket.stomp.onclose = reconnect;
        };

        initialize();
        return service;
    })


    .service("NotificationsService", function($q, $timeout) {

        var service = {}, listener = $q.defer(), socket = {
            client: null,
            stomp: null
        }, messageIds = [];

        service.RECONNECT_TIMEOUT = 30000;
        service.SOCKET_URL = "/unread";
        service.CHAT_TOPIC = "/user/topic/unread";
        service.CHAT_BROKER = "/app/unread";

        service.receive = function() {
            return listener.promise;
        };

        service.send = function(message) {
            socket.stomp.send(service.CHAT_BROKER, {
                priority: 9
            }, JSON.stringify(""));
        };

        var reconnect = function() {
            $timeout(function() {
                initialize();
            }, this.RECONNECT_TIMEOUT);
        };

        var getMessage = function(data) {
            return JSON.parse(data);
        };

        var startListener = function() {
            socket.stomp.subscribe(service.CHAT_TOPIC, function(data) {
                listener.notify(getMessage(data.body));
            });
            service.send('asd');
        };

        var initialize = function() {
            socket.client = new SockJS(service.SOCKET_URL);
            socket.stomp = Stomp.over(socket.client);
            socket.stomp.connect({}, startListener);
            socket.stomp.onclose = reconnect;
        };

        initialize();
        return service;
    })


    .service("ReadMessageService", function($q, $timeout) {

        var service = {}, listener = $q.defer(), socket = {
            client: null,
            stomp: null
        }, messageIds = [];

        service.RECONNECT_TIMEOUT = 30000;
        service.SOCKET_URL = "/readMessage";
        service.CHAT_TOPIC = "/somethingNeverWillEverUse";
        service.CHAT_BROKER = "/app/readMessage";

        service.receive = function() {
            return listener.promise;
        };

        service.send = function(message) {
            socket.stomp.send(service.CHAT_BROKER, {
                priority: 9
            }, JSON.stringify(message));
        };

        var reconnect = function() {
            $timeout(function() {
                initialize();
            }, this.RECONNECT_TIMEOUT);
        };

        //var getMessage = function(data) {
        //    return JSON.parse(data);
        //};

        var startListener = function() {
            socket.stomp.subscribe(service.CHAT_TOPIC, function(data) {
                listener.notify(getMessage(data.body));
            });
        };

        var initialize = function() {
            socket.client = new SockJS(service.SOCKET_URL);
            socket.stomp = Stomp.over(socket.client);
            socket.stomp.connect({}, startListener);
            socket.stomp.onclose = reconnect;
        };

        initialize();
        return service;
    });