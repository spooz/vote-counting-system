
sortMessages =  function($scope) {
    for( var i=0; i<$scope.conversations.length; i++ ) {
        document.getElementById('chat_window_' + $scope.conversations[i].intercutour).style.left=
            (40 + 300*i) + 'px';
    }
};

addNotification = function($scope, message) {
    var index = -1;
    for(var j=0; j<$scope.messageNotifications.length; j++) {
        if($scope.messageNotifications[j].senderId == message.senderId) {
            $scope.messageNotifications[j].date = message.date;
            $scope.messageNotifications[j].message = message.message;
            index = j;
        }

    }
    if(index == -1) {
        $scope.messageNotifications.push(message);
    }
};



angular.module("chatApp.controllers").controller("ChatController", function($scope, ChatService, AccessibleUsersService,
                                                OpenConversationService, NotificationsService, ReadMessageService) {
    $scope.messages = [];
    $scope.message = "";
    $scope.max = 140;

    $scope.addMessage = function(message, name) {
        if(message.length != 0) {
            ChatService.send(message, name);
            for (var i = 0; i < $scope.conversations.length; i++) {
                if ($scope.conversations[i].intercutour == name) {
                    $scope.conversations[i].newmessage = "";
                }
            }
        }
    };

    ChatService.receive().then(null, null, function(message) {
        var index = -1;
        for(var i=0; i<$scope.conversations.length; i++) {
            if($scope.conversations[i].intercutour == message.receiverName ||
                $scope.conversations[i].intercutour == message.senderName) {
                index = i;
            }
        }

        if(index != -1) {
            message.sent = message.senderName != $scope.conversations[index].intercutour;
            $scope.conversations[index].messages.push(message);
            $scope.markMessageAsRead(message.id);
        } else {
            addNotification($scope, message);
        }
    });

    $scope.loadAccessibleUserList = function(message) {
        if($scope.accessableUsers.length == 0)
            AccessibleUsersService.send(message);
    };

    AccessibleUsersService.receive().then(null, null, function(message) {
        $scope.accessableUsers = message;
    });

    $scope.openConversation = function(id, name) {
        var isOpen = false;
        for(var i=0; i<$scope.conversations.length; i++) {
            if($scope.conversations[i].intercutour == name) {
                isOpen = true
            }
        }

        if(!isOpen) {
            $scope.conversations.push({intercutour:name, messages:[]});
            OpenConversationService.send(name);
            for(i=0; i<$scope.messageNotifications.length; i++) {
                if($scope.messageNotifications[i].senderName == name) {
                    $scope.messageNotifications.splice(i,1);
                }
            }
        }
    };

    OpenConversationService.receive().then(null, null, function(message) {
        var index = -1;
        for(var i=0; i<$scope.conversations.length; i++) {
            if($scope.conversations[i].intercutour == message[0].receiverName ||
                $scope.conversations[i].intercutour == message[0].senderName) {
                index = i;
            }
        }

        if(index != -1) {
            for(i=0; i<message.length; i++) {
                message[i].sent = message[i].receiverName == $scope.conversations[index].intercutour;
                $scope.conversations[index].messages.push(message[i]);
                if(!message[i].sent && !message[i].read) {
                    $scope.markMessageAsRead(message[i].id);
                }
            }
        }
    });

    //$scope.getNotifications = function(id, name) {
    //    if($scope.messageNotifications.length == 0)
    //        NotificationsService.send(message);
    //};

    NotificationsService.receive().then(null, null, function(message) {
        for(var i=0; i<message.length; i++) {
            addNotification($scope, message[i]);
        }
    });

    $scope.markMessageAsRead = function(id) {
        ReadMessageService.send(id);
    };





    $scope.messageNotifications = [];

    $scope.conversations = [];

    $scope.accessableUsers = [];

    $scope.closeConversation = function(name){
        for( var i=0; i<$scope.conversations.length; i++ ) {
            if($scope.conversations[i].intercutour == name)
                $scope.conversations.splice(i,1);
        }
        sortMessages($scope);
    };

});