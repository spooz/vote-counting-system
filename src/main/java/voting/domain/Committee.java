package voting.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "committee")
public class Committee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "full_name", nullable = false, unique = true)
    private String fullName;

    @Column(name = "short_name", nullable = false, unique = true)
    private String shortName;

    @OneToMany(mappedBy = "committee")
    private List<VotingList> votingLists;

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<VotingList> getVotingLists() {
        return votingLists;
    }

    public void setVotingLists(List<VotingList> votingLists) {
        this.votingLists = votingLists;
    }

    @Override
    public String toString() {
        return "Committee{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", shortName='" + shortName + '\'' +
                '}';
    }
}
