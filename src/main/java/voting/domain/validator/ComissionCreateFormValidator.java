package voting.domain.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import voting.domain.Comission;
import voting.domain.ComissionType;
import voting.domain.Role;
import voting.domain.forms.ComissionCreateForm;
import voting.domain.forms.UserCreateForm;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;


@Component
public class ComissionCreateFormValidator implements Validator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ComissionCreateFormValidator.class);
    private final ComissionService comissionService;
    private final CurrentUserDetailsService currentUserDetailsService;

    @Autowired
    public ComissionCreateFormValidator(ComissionService comissionService, CurrentUserDetailsService currentUserDetailsService) {
        this.comissionService = comissionService;
        this.currentUserDetailsService = currentUserDetailsService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(ComissionCreateForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LOGGER.debug("Validating {}", target);
        ComissionCreateForm form = (ComissionCreateForm) target;
        validateCode(errors, form);
        validatePermissions(errors, form);
        validateParentPermissions(errors, form);
    }

    //Comission can't have 2 subcomissions with same code
    private void validateCode(Errors errors, ComissionCreateForm form) {
        if(form.getParentComission() != null)
            for(Comission com : comissionService.getComissionsByParentComission(form.getParentComission()))
                if (com.getCode() == form.getCode()) {
                    errors.reject("kod.exists", "Comission with this kod already exists");
                    return;
                }
    }

    //Only admin is allowed to create new comission
    private void validatePermissions(Errors errors, ComissionCreateForm form) {
        if(currentUserDetailsService.getCurrentUser().getRole() != Role.ADMIN) {
            errors.reject("permision.notgranted", "You don't have permission to create this comission");
        }
    }

    //PKW must have null as parent comission, other must have comission which are one type higher in hierarchy
    private void validateParentPermissions(Errors errors, ComissionCreateForm form) {
        if((form.getParentComission() != null &&
                form.getParentComission().getComissionType().compareTo(form.getComissionType()) != -1)
                || (form.getParentComission() == null && form.getComissionType() != ComissionType.PKW)) {
            errors.reject("permision.notgranted", "Comission have invalid parent");
        }
    }
}
