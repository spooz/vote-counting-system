package voting.domain.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import voting.domain.ComissionType;
import voting.domain.ElectionType;
import voting.domain.Role;
import voting.domain.VotingList;
import voting.domain.forms.VotingListForm;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;
import voting.service.election.ElectionService;
import voting.service.votingList.VotingListService;

/**
 * Created by Bartosz on 2016-04-03.
 */
@Component
public class VotingListFormValidator implements Validator{

    private static final Logger LOGGER = LoggerFactory.getLogger(VotingListFormValidator.class);
    private final VotingListService votingListService;
    private final ComissionService comissionService;
    private final ElectionService electionService;
    private final CurrentUserDetailsService currentUserDetailsService;

    @Autowired
    public VotingListFormValidator(VotingListService votingListService, ComissionService comissionService, ElectionService electionService, CurrentUserDetailsService currentUserDetailsService) {
        this.votingListService = votingListService;
        this.comissionService = comissionService;
        this.electionService = electionService;
        this.currentUserDetailsService = currentUserDetailsService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(VotingListForm.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        VotingListForm form = (VotingListForm) o;
        validateComission(errors,form);
        validateElection(errors,form);
        validateCommittee(errors,form);
        validateListNumber(errors,form);
    }

    public void validateListNumber(Errors errors, VotingListForm form) {
        if(currentUserDetailsService.getCurrentUser().getRole() == Role.PKW) {
            if (votingListService.findByComissionAndElection(form.getComission(), form.getElection()).isEmpty() == false) {
                errors.reject("permission.not_granted", "This type of election can have only one voting list.");
            }
            form.setListNumber("1");
        } else if(currentUserDetailsService.getCurrentUser().getRole() == Role.OKW) {
            if(form.getListNumber().isEmpty() || Long.parseLong(form.getListNumber()) <= 0)
            {
                errors.reject("permission.not_granted", "Voting list needs to have a positive number.");
            } else {
                for (VotingList votingList : votingListService.findByComissionAndElection(form.getComission(), form.getElection())) {
                    if (votingList.getListNumber() == Long.parseLong(form.getListNumber())) {
                        errors.reject("permission.not_granted", "Voting list with this number already exists.");
                    }
                }
            }
        } else {
            errors.reject("permission.not_granted", "You have no permission.");
        }

    }

    public void validateComission(Errors errors, VotingListForm form) {
        form.setComission(currentUserDetailsService.getCurrentUser().getUser().getComission());
    }

    public void validateElection(Errors errors, VotingListForm form) {
        if(currentUserDetailsService.getCurrentUser().getRole() == Role.PKW) {
            if (form.getElection().getElectionType() == ElectionType.DISTRICT) {
                errors.reject("permission.not_granted", "You have no permission to create voting list for this election.");
            }
        } else if(currentUserDetailsService.getCurrentUser().getRole() == Role.OKW) {
            if (form.getElection().getElectionType() == ElectionType.PRESIDENTIAL) {
                errors.reject("permission.not_granted", "You have no permission to create voting list for this election.");
            }
        } else {
            errors.reject("permission.not_granted", "You have no permission.");
        }
    }

    public void validateCommittee(Errors errors, VotingListForm form) {
        if(currentUserDetailsService.getCurrentUser().getRole() == Role.PKW) {
            form.setCommittee(null);
        } else if(currentUserDetailsService.getCurrentUser().getRole() == Role.OKW) {
            for(VotingList votingList : votingListService.findByComissionAndElection(form.getComission(), form.getElection())) {
                if(votingList.getCommittee() == form.getCommittee()) {
                    errors.reject("permission.not_granted", "Voting list with this committee already exists.");
                }
            }
        } else {
            errors.reject("permission.not_granted", "You have no permission.");
        }

    }
}
