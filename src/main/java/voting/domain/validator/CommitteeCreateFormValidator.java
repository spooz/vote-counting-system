package voting.domain.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import voting.domain.Role;
import voting.domain.forms.CommitteeCreateForm;
import voting.service.committee.CommitteeService;
import voting.service.currentuser.CurrentUserDetailsService;

@Component
public class CommitteeCreateFormValidator implements Validator {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommitteeCreateFormValidator.class);
    private final CommitteeService committeeService;
    private final CurrentUserDetailsService currentUserDetailsService;

    @Autowired
    public CommitteeCreateFormValidator(CommitteeService committeeService, CurrentUserDetailsService currentUserDetailsService) {
        this.committeeService = committeeService;
        this.currentUserDetailsService = currentUserDetailsService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(CommitteeCreateForm.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        LOGGER.debug("Validating {}", o);
        CommitteeCreateForm form = (CommitteeCreateForm) o;
        validatePermissions(errors, form);
        validateFullName(errors, form);
        validateShortName(errors, form);
    }

    private void validatePermissions(Errors errors, CommitteeCreateForm form) {
        if (currentUserDetailsService.getCurrentUser().getRole() != Role.PKW)
            errors.reject("permision.notgranted", "You don't have permission to create committe");
    }

    private void validateFullName(Errors errors, CommitteeCreateForm form) {
        if (committeeService.getCommitteeByFullName(form.getFullName()).isPresent())
            errors.reject("full_name.exists", "Committee with this full name already exists");
    }

    private void validateShortName(Errors errors, CommitteeCreateForm form) {
        if (committeeService.getCommitteeByShortName(form.getShortName()).isPresent())
            errors.reject("short_name.exists", "Committee with this short name already exists");
    }
}
