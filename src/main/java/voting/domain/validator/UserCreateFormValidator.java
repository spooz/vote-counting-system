package voting.domain.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import voting.domain.Comission;
import voting.domain.Role;
import voting.domain.forms.UserCreateForm;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;
import voting.service.user.UserService;

@Component
public class UserCreateFormValidator implements Validator {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserCreateFormValidator.class);
    private final UserService userService;
    private final ComissionService comissionService;
    private final CurrentUserDetailsService currentUserDetailsService;

    @Autowired
    public UserCreateFormValidator(UserService userService, ComissionService comissionService, CurrentUserDetailsService currentUserDetailsService) {
        this.userService = userService;
        this.comissionService = comissionService;
        this.currentUserDetailsService = currentUserDetailsService;
    }


    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(UserCreateForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LOGGER.debug("Validating {}", target);
        UserCreateForm form = (UserCreateForm) target;
        validatePasswords(errors, form);
        validateLogin(errors, form);
        validatePermissions(errors,form);
        validateComission(errors,form);
    }

    //Password and PasswordRepeated have to match
    private void validatePasswords(Errors errors, UserCreateForm form) {
        if(form.getRole() != Role.ADMIN && form.getComission() == null) {
            errors.reject("permission.not_granted", "That type of user requires comission");
        }
        if(currentUserDetailsService.getCurrentUser().getRole() != Role.ADMIN && form.getComission() != null &&
                form.getComission().getComissionType().compareTo(
                        currentUserDetailsService.getCurrentUser().getUser().getComission().getComissionType()) != 1) {
            errors.reject("permission.not_granted", "You don't have permission to create user with this role");
        }
    }

    //Login must be unique
    private void validateLogin(Errors errors, UserCreateForm form) {
        if (userService.getUserByLogin(form.getLogin()).isPresent()) {
            errors.reject("login.exists", "User with this login already exists");
        }
    }

    //Setting user role by creator's role / comission's role
    private void validatePermissions(Errors errors, UserCreateForm form) {
        switch (currentUserDetailsService.getCurrentUser().getRole()) {
            case PKW:
                form.setRole(Role.OKW);
                break;
            case OKW:
                form.setRole(Role.OBWKW);
                break;
            case ADMIN:
                if(form.getComission() == null)
                    form.setRole(Role.ADMIN);
                else {
                    switch (form.getComission().getComissionType()) {
                        case PKW:
                            form.setRole(Role.PKW);
                            break;
                        case OKW:
                            form.setRole(Role.OKW);
                            break;
                        case ObKW:
                            form.setRole(Role.OBWKW);
                            break;
                    }
                }
                break;
        }
    }

    //Everyone but admin requires comission, admin mustn't have comission, users can create only member of it's subcomissions
    private void validateComission(Errors errors, UserCreateForm form) {
        LOGGER.debug("Validating ");
        if(form.getRole() != Role.ADMIN && form.getComission() == null) {
            errors.reject("permission.not_granted", "That type of user requires comission");
        } else if(form.getRole() == Role.ADMIN && form.getComission() != null) {
            errors.reject("permission.not_granted", "That type of user can't have comission");
        } else if(currentUserDetailsService.getCurrentUser().getRole() != Role.ADMIN && form.getComission() != null) {
            for (Comission com : comissionService.getComissionsByParentComission(
                    currentUserDetailsService.getCurrentUser().getUser().getComission()))
                if (com.getId() == form.getComission().getId())
                    return;
            errors.reject("permission.not_granted", "You don't have permission to create user with this role");
        }
    }
}
