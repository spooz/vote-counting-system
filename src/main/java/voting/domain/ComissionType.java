package voting.domain;

public enum ComissionType {
    PKW, OKW, ObKW
}
