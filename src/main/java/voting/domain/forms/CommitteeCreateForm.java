package voting.domain.forms;

import org.hibernate.validator.constraints.NotEmpty;

public class CommitteeCreateForm {

    @NotEmpty
    private String fullName = "";

    @NotEmpty
    private String shortName = "";

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Override
    public String toString() {
        return "CommitteeCreateForm{" +
                "fullName='" + fullName + '\'' +
                ", shortName='" + shortName + '\'' +
                '}';
    }
}
