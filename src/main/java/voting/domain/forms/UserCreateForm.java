package voting.domain.forms;


import org.hibernate.validator.constraints.NotEmpty;
import voting.domain.Comission;
import voting.domain.Role;

import javax.validation.constraints.NotNull;

public class UserCreateForm {

    @NotEmpty
    private String login = "";

    @NotEmpty
    private String password = "";

    @NotEmpty
    private String passwordRepeated = "";

    @NotNull
    private Role role = Role.OBWKW;

    private Comission comission = null;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeated() {
        return passwordRepeated;
    }

    public void setPasswordRepeated(String passwordRepeated) {
        this.passwordRepeated = passwordRepeated;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Comission getComission() {
        return comission;
    }

    public void setComission(Comission comission) {
        this.comission = comission;
    }

    @Override
    public String toString() {
        return "UserCreateForm{" +
                "login='" + login + '\'' +
                ", password=***" + '\'' +
                ", passwordRepeated=***" + '\'' +
                ", role=" + role +
                ", comission= " + comission +
                '}';
    }
}