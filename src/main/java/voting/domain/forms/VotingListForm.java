package voting.domain.forms;

import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import voting.domain.Comission;
import voting.domain.Committee;
import voting.domain.Election;

/**
 * Created by Bartosz on 2016-04-03.
 */
public class VotingListForm {

    private String listNumber;

    private Comission comission;

    private Committee committee;

    @NotNull
    private Election election;

    public String getListNumber() {
        return listNumber;
    }

    public void setListNumber(String listNumber) {
        this.listNumber = listNumber;
    }

    public Comission getComission() {
      return  comission;
    }

    public Committee getCommittee() {
        return  committee;
    }

    public Election getElection() {
        return  election;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public void setCommittee(Committee committee) {
        this.committee = committee;
    }

    public void setComission(Comission comission) {
        this.comission = comission;
    }



}
