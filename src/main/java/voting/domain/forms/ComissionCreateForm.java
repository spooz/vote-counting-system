package voting.domain.forms;

import org.hibernate.validator.constraints.NotEmpty;
import voting.domain.Comission;
import voting.domain.ComissionType;

import javax.validation.constraints.NotNull;

public class ComissionCreateForm {

    @NotNull
    private Long code = 0L;

    @NotEmpty
    private String address = "";

    @NotNull
    private ComissionType comissionType = ComissionType.ObKW;

    private Comission parentComission = null;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ComissionType getComissionType() {
        return comissionType;
    }

    public void setComissionType(ComissionType comissionType) {
        this.comissionType = comissionType;
    }

    public Comission getParentComission() {
        return parentComission;
    }

    public void setParentComission(Comission parentComission) {
        this.parentComission = parentComission;
    }

    @Override
    public String toString() {
        if(parentComission != null) {
            return "ComissionCreateForm{" +
                    "code=" + code +
                    ", address='" + address + '\'' +
                    ", comissionType=" + comissionType +
                    ", parentComission=" + parentComission.getId() +
                    '}';
        } else {
            return "ComissionCreateForm{" +
                    "code=" + code +
                    ", address='" + address + '\'' +
                    ", comissionType=" + comissionType +
                    ", parentComission=" +
                    '}';
        }
    }
}
