package voting.domain;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Bartosz on 2016-03-23.
 */
@Entity
@Table(name="election")
public class Election {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @OneToMany(mappedBy = "election")
    private List<VotingList> votingLists;


    @Column(name = "election_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ElectionType electionType;

    @Column(name = "name")
    private String name;

    @Column(name = "active")
    private Boolean isActive = true;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private Date startDate;

    @Column(name = "end_time")
    @DateTimeFormat(pattern = "dd/mm/yyyy")
    private Date endDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ElectionType getElectionType() {
        return electionType;
    }

    public void setElectionType(ElectionType electionType) {
        this.electionType = electionType;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return  name;
    }

    public List<VotingList> getVotingLists() {
        return this.votingLists;
    }

    public void setVotingLists(List<VotingList> votingLists) {
        this.votingLists = votingLists;
    } 

    public void setIsActive(boolean active) {
        this.isActive = active;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

}
