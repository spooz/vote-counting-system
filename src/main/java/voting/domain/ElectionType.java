package voting.domain;

/**
 * Created by Bartosz on 2016-04-17.
 */
public enum ElectionType {
    DISTRICT, PRESIDENTIAL
}
