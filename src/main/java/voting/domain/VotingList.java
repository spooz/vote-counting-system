package voting.domain;

import javax.persistence.*;
import javax.persistence.criteria.Fetch;
import java.util.List;

@Entity
@Table(name="voting_list")
public class VotingList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "list_number")
    private Long listNumber;

    @OneToMany(mappedBy = "votingList", fetch = FetchType.EAGER)
    private List<Candidate> candidates;

    @ManyToOne
    private Comission comission;

    @ManyToOne
    private Committee committee;

    @ManyToOne
    private Election election;

    public Long getId() {
        return id;
    }

    public Long getListNumber() {
        return listNumber;
    }

    public void setListNumber(Long listNumber) {
        this.listNumber = listNumber;
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }

    public Comission getComission() {
        return comission;
    }

    public void setComission(Comission comission) {
        this.comission = comission;
    }

    public Committee getCommittee() {
        return committee;
    }

    public void setCommittee(Committee committee) {
        this.committee = committee;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public Election getElection() {
        return this.election;
    }
}
