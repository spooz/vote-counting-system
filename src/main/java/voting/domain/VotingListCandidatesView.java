package voting.domain;

import java.util.List;

/**
 * Created by Bartek on 12.06.2016.
 */
public class VotingListCandidatesView {

    private List<VotesResult> votesResults;

    private VotingList votingList;

    private int allComissions;

    public VotingListCandidatesView(VotingList votingList, List<VotesResult> votesResults, int allComissions) {
        this.votingList = votingList;
        this.setVotesResults(votesResults);
        this.allComissions = allComissions;
    }

    public VotingList getVotingList() {
        return votingList;
    }

    public void setVotingList(VotingList votingList) {
        this.votingList = votingList;
    }


    public List<VotesResult> getVotesResults() {
        return votesResults;
    }

    public void setVotesResults(List<VotesResult> votesResults) {
        this.votesResults = votesResults;
    }

    public int getAllComissions() {
        return allComissions;
    }

    public void setAllComissions(int allComissions) {
        this.allComissions = allComissions;
    }
}
