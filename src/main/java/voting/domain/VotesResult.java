package voting.domain;

public class VotesResult {

    private Candidate candidate;
    private int votes;
    private int comissions;

    public VotesResult(Candidate candidate, int votes, int comissions) {
        this.candidate = candidate;
        this.votes = votes;
        this.comissions = comissions;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public int getComissions() {
        return comissions;
    }

    public void setComissions(int comissions) {
        this.comissions = comissions;
    }
}
