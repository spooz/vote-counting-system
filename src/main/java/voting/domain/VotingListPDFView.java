package voting.domain;

import java.util.Map;

/**
 * Created by Bartek on 12.06.2016.
 */
public class VotingListPDFView {

    public VotingListPDFView(Comission comission, VotingList votingList, Map<Candidate, Integer> votes, Integer votesCount) {
        this.votingList = votingList;
        this.votes = votes;
        this.votesCount = votesCount;
        this.comission = comission;
    }

    private VotingList votingList;
    private Map<Candidate, Integer> votes;
    private Integer votesCount;
    private Comission comission;

    public Comission getComission() {
        return comission;
    }

    public void setComission(Comission comission) {
        this.comission = comission;
    }

    public Integer getVotesCount() {
        return votesCount;
    }

    public void setVotesCount(Integer votesCount) {
        this.votesCount = votesCount;
    }

    public Map<Candidate, Integer> getVotes() {
        return votes;
    }

    public void setVotes(Map<Candidate, Integer> votes) {
        this.votes = votes;
    }

    public VotingList getVotingList() {
        return votingList;
    }

    public void setVotingList(VotingList votingList) {
        this.votingList = votingList;
    }
}
