package voting.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "comission")
public class Comission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "code", nullable = false)
    private Long code;

    @Column(name = "address", nullable = false, unique = true)
    private String address;

    @Column(name = "comission_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private ComissionType comissionType;

    @OneToMany(mappedBy = "comission", cascade = CascadeType.ALL)
    private List<User> users;

    @ManyToOne
    private Comission parentComission;

    @OneToMany(mappedBy = "parentComission")
    private List<Comission> comissions;

    @OneToMany(mappedBy = "comission")
    private List<VotingList> votingLists;

    @OneToMany(mappedBy = "comission")
    private List<Votes> votes;

    public List<Votes> getVotes() {
        return votes;
    }

    public void setVotes(List<Votes> votes) {
        this.votes = votes;
    }

    public Long getId() {
        return id;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ComissionType getComissionType() {
        return comissionType;
    }

    public void setComissionType(ComissionType comissionType) {
        this.comissionType = comissionType;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Comission getParentComission() {
        return parentComission;
    }

    public void setParentComission(Comission parentComission) {
        this.parentComission = parentComission;
    }

    public List<VotingList> getVotingLists() {
        return votingLists;
    }

    public void setVotingLists(List<VotingList> votingLists) {
        this.votingLists = votingLists;
    }


    public List<Comission> getComissions() {
        return comissions;
    }

    public void setComissions(List<Comission> comissions) {
        this.comissions = comissions;
    }

    @Override
    public String toString() {
        return "Comission{" +
                "id=" + id +
                ", code=" + code +
                ", address='" + address + '\'' +
                ", comissionType=" + comissionType +
                '}';
    }
}
