package voting.domain;

import javax.persistence.*;

@Entity
@Table(name="user")
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable = false, updatable=false)
	private Long id;
	
	@Column(name="login", nullable=false, unique = true)
	private String login;
	
	@Column(name="password_hash", nullable=false)
	private String passwordHash;
	
	@Column(name="role", nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;

    @ManyToOne
    private Election election;

    @ManyToOne
    private Comission comission;
	
	public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Comission getComission() {
        return comission;
    }

    public void setComission(Comission comission) {
        this.comission = comission;
    }

    public void setElection(Election election) {
        this.election = election;
    }

    public Election getElection() {
        return this.election;
    }

    @Override
    public String toString() {
        if(comission != null) {
            return "User{" +
                    "id=" + id +
                    ", login='" + login +
                    ", passwordHash='" + passwordHash.substring(0, 10) +
                    ", role=" + role +
                    ", comission=" + comission.getId() +
                    '}';
        } else {
            return "User{" +
                    "id=" + id +
                    ", login='" + login +
                    ", passwordHash='" + passwordHash.substring(0, 10) +
                    ", role=" + role +
                    ", comission=" +
                    '}';
        }
    }
}
