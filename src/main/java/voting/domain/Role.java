package voting.domain;

public enum Role {
	ADMIN, PKW, OKW, OBWKW;
}
