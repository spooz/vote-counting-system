package voting.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import voting.domain.Comission;
import voting.domain.Election;
import voting.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByLogin(String login);

    Collection<User> findByComission(Comission comission);

    @Modifying
    @Query(value = "UPDATE User u  SET u.election = :election WHERE u = :user")
    void setUserCurrentElection(@Param(value="user") User user, @Param(value="election") Election election);
}