package voting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import voting.domain.Candidate;
import voting.domain.VotingList;

import java.util.Collection;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {
    Collection<Candidate> findByVotingList(VotingList votingList);
    Collection<Candidate> findByLastName(String lastName);
    Candidate findById(Long id);
}
