package voting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import voting.domain.Election;

import java.util.List;

/**
 * Created by Bartosz on 2016-03-23.
 */
@Repository
public interface ElectionRepository extends JpaRepository<Election, Long> {

    Election findById(Long id);
    List<Election> findByIsActive(boolean isActive);
}
