package voting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import voting.domain.Comission;
import voting.domain.ComissionType;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface ComissionRepository extends JpaRepository<Comission, Long> {
    Optional<Comission> findOneByCode(Long code);

    Collection<Comission> findAllByParentComission(Comission comission);

    List<Comission> findByComissionType(ComissionType comissionType);

}
