package voting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import voting.domain.Candidate;
import voting.domain.Comission;
import voting.domain.Votes;

import java.util.List;

/**
 * Created by KOZAK on 2016-05-09.
 */

@Repository
public interface VotesRepository extends JpaRepository<Votes, Long> {
    List<Votes> findByComission(Comission comission);
    List<Votes> findByCandidate(Candidate candidate);
    Votes findByComissionAndCandidate(Comission comission, Candidate candidate);
    Votes findById(Long id);


    @Query(value = "SELECT sum(v.amount) FROM Votes v JOIN v.comission com JOIN com.parentComission pCom WHERE pCom.id = :comissionId")
    Integer getVotesCountForOKW(@Param(value = "comissionId") Long comissionId);

    @Query(value = "SELECT sum(v.amount) FROM Votes v JOIN v.comission com JOIN com.parentComission pCom JOIN pCom.parentComission ppCom WHERE ppCom.id = :comissionId")
    Integer getVotesCountForPKW(@Param(value = "comissionId") Long comissionId);

    @Query(value = "SELECT sum(v.amount) FROM Votes v JOIN v.candidate c JOIN c.votingList vl WHERE vl.id = :votingList GROUP BY vl.id")
    Integer getVotesCountForVotingList(@Param(value = "votingList") Long votingList);

    @Query(value = "SELECT sum(v.amount) FROM Votes v JOIN v.candidate c WHERE c.id = :candidate")
    Integer getVotesCountForCandidate(@Param(value = "candidate") Long candidate);

    @Query(value = "SELECT count(v) FROM Votes v JOIN v.candidate c WHERE c.id = :candidate")
    Integer getComissionsCountForCandidate(@Param(value = "candidate") Long candidate);

    @Query(value = "SELECT sum(v.amount) FROM Votes v JOIN v.candidate c JOIN v.comission.parentComission c1 WHERE c.id = :candidate AND c1.id = :comission")
    Integer getVotesCountForCandidateFromSubordinates(@Param(value = "candidate") Long candidate, @Param(value = "comission") Long comission);

    @Query(value = "SELECT count(v) FROM Votes v JOIN v.candidate c JOIN v.comission.parentComission c1 WHERE c.id = :candidate AND c1.id = :comission")
    Integer getComissionsCountForCandidateFromSubordinates(@Param(value = "candidate") Long candidate, @Param(value = "comission") Long comission);
}
