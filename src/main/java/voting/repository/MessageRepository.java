package voting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import voting.comm.Message;

import java.util.Collection;
import java.util.Optional;

public interface MessageRepository extends JpaRepository<Message, Long> {
    Optional<Message> findById(Long id);
    Collection<Message> findByReceiverId(Long receiverId);
    Collection<Message> findBySenderId(Long senderId);
    Collection<Message> findByIsRead(boolean isRead);
}
