package voting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import voting.domain.Committee;

import java.util.Optional;

@Repository
public interface CommitteeRepository extends JpaRepository<Committee, Long> {
    Optional<Committee> findOneByFullName(String fullName);
    Optional<Committee> findOneByShortName(String shortName);
}
