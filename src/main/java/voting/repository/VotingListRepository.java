package voting.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import voting.domain.Comission;
import voting.domain.Election;
import voting.domain.VotingList;

import java.util.Collection;
import java.util.List;

@Repository
public interface VotingListRepository extends JpaRepository<VotingList, Long> {
    List<VotingList> findByComissionAndElection(Comission comission, Election election);

}
