package voting.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import voting.domain.Election;
import voting.service.election.ElectionService;

/**
 * Created by Bartosz on 2016-03-23.
 */
@Component
public class ElectionConverter implements Converter<Long, Election> {


    @Autowired
    private ElectionService electionService;
    @Override
    public Election convert(Long id) {
        return electionService.findById(id);
    }
}
