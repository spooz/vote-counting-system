package voting.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import voting.domain.VotingList;
import voting.service.votingList.VotingListService;

/**
 * Created by Bartosz on 2016-04-17.
 */
@Component
public class VotingListConverter implements Converter<Long, VotingList> {

    @Autowired
    private VotingListService votingListService;

    @Override
    public VotingList convert(Long id) {
       return votingListService.findById(id);
    }
}
