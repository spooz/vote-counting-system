package voting.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import voting.domain.Comission;
import voting.service.comission.ComissionService;

/**
 * Created by Bartosz on 2016-03-23.
 */
@Component
public class ComissionConverter implements Converter<Long, Comission> {

    @Autowired
    private ComissionService comissionService;

    @Override
    public Comission convert(Long id) {
        return comissionService.getComissionById(id).get();
    }
}
