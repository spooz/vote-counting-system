package voting.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import voting.domain.Candidate;
import voting.domain.Comission;
import voting.service.candidate.CandidateService;

/**
 * Created by Bartosz on 2016-04-17.
 */
@Component
public class CandidateConverter  implements Converter<Long, Candidate> {

    @Autowired
     private CandidateService candidateService;

    @Override
    public Candidate convert(Long id) {
        return candidateService.findById(id);
    }
}
