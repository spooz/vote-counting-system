package voting.service.currentuser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import voting.domain.*;
import voting.service.comission.ComissionService;
import voting.service.user.UserService;
import voting.service.votingList.VotingListService;

import java.util.Collection;
import java.util.Objects;

@Service
public class CurrentUserServiceImpl implements CurrentUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentUserDetailsService.class);
    private final ComissionService comissionService;
    private final UserService userService;

    @Autowired
    public CurrentUserServiceImpl(ComissionService comissionService, UserService userService) {
        this.comissionService = comissionService;
        this.userService = userService;
    }

    //Admin can access every user, every user can access himself and members of his subcomissions
    @Override
    public boolean canAccessUser(CurrentUser currentUser, Long userId) {
        LOGGER.debug("Checking if user={} has access to user={}", currentUser, userId);
        return currentUser != null
                && (currentUser.getRole() == Role.ADMIN || currentUser.getId().equals(userId) ||
                currentUser.getUser().getComission().getId().equals(
                        userService.getUserById(userId).get().getComission().getParentComission().getId()));
    }

    //Admin can access every comission, every user can access his comission and his subcomissions
    @Override
    public boolean canAccessComission(CurrentUser currentUser, Long comissionId) {
        LOGGER.debug("Checking if user={} has access to comission={}", currentUser, comissionId);
        return currentUser != null
                && (currentUser.getRole() == Role.ADMIN || Objects.equals(currentUser.getUser().getComission().getId(), comissionId) ||
                currentUser.getUser().getComission().getId().equals(
                        comissionService.getComissionById(comissionId).get().getParentComission().getId()));
    }

    @Override
    public boolean canAccessVotingList(CurrentUser currentUser, VotingList votingList) {
        LOGGER.debug("Checking if user={} has access to votingList={}", currentUser, votingList.getId());
        return currentUser != null
                && (currentUser.getRole() == Role.PKW
                    || Objects.equals(currentUser.getUser().getComission().getId(), votingList.getComission().getId())
                    || currentUser.getUser().getComission().getParentComission().getId().equals(
                       votingList.getComission().getId()));
    }

    @Override
    public Collection<User> getAccessableUsers(User user) {
        LOGGER.debug("Getting accessable users for user={}", user);
        Collection<User> list = null;
        switch (user.getRole()) {
            case ADMIN:
                list = userService.getAllUsers();
                break;
            case PKW:
                list = userService.getUsersByComission(user.getComission());
                for (Comission comission: comissionService.getComissionsByParentComission(user.getComission())) {
                    list.addAll(userService.getUsersByComission(comission));
                }
                list.addAll(userService.getUsersByComission(null));
                break;
            case OKW:
                list = userService.getUsersByComission(user.getComission());
                list.addAll(userService.getUsersByComission(user.getComission().getParentComission()));
                for (Comission comission: comissionService.getComissionsByParentComission(user.getComission())) {
                    list.addAll(userService.getUsersByComission(comission));
                }
                list.addAll(userService.getUsersByComission(null));
                break;
            case OBWKW:
                list = userService.getUsersByComission(user.getComission());
                list.addAll(userService.getUsersByComission(user.getComission().getParentComission()));
                list.addAll(userService.getUsersByComission(null));
                break;
        }

        return list;
    }

    @Override
    public boolean canCreateCandidate(CurrentUser currentUser, VotingList votingList) {
        switch (currentUser.getUser().getRole()) {
            case PKW:
                if (votingList.getElection().getElectionType() == ElectionType.PRESIDENTIAL)
                    return true;
                break;
            case OKW:
                if (votingList.getElection().getElectionType() == ElectionType.DISTRICT)
                    return true;
                break;
            default:
        }
        return false;
    }

    @Override
    public boolean canAccessCandidate(CurrentUser currentUser, Candidate candidate) {
        switch (currentUser.getUser().getRole()) {
            case PKW:
                if (candidate.getVotingList().getElection().getElectionType() == ElectionType.PRESIDENTIAL)
                    return true;
                break;
            case OKW:
                if (candidate.getVotingList().getElection().getElectionType() == ElectionType.DISTRICT)
                    return true;
                break;
            default:
        }
        return false;
    }

}