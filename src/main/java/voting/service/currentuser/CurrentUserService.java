package voting.service.currentuser;

import voting.domain.Candidate;
import voting.domain.CurrentUser;
import voting.domain.User;
import voting.domain.VotingList;

import java.util.Collection;

public interface CurrentUserService {

    boolean canAccessUser(CurrentUser currentUser, Long userId);
    boolean canAccessComission(CurrentUser currentUser, Long comissionId);
    boolean canAccessVotingList(CurrentUser currentUser, VotingList votingListId);
    boolean canCreateCandidate(CurrentUser currentUser, VotingList votingListId);
    boolean canAccessCandidate(CurrentUser currentUser, Candidate candidate);

    Collection<User> getAccessableUsers(User user);

}
