package voting.service.committee;

import voting.domain.Committee;
import voting.domain.forms.CommitteeCreateForm;

import java.util.Collection;
import java.util.Optional;

public interface CommitteeService {
    Optional<Committee> getCommitteeById(Long id);

    Optional<Committee> getCommitteeByFullName(String fullName);

    Optional<Committee> getCommitteeByShortName(String shortName);

    Collection<Committee> getAllCommittees();

    Committee create(CommitteeCreateForm form);

    void delete(Long id);
}
