package voting.service.committee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import voting.domain.Committee;
import voting.domain.forms.CommitteeCreateForm;
import voting.repository.CommitteeRepository;

import java.util.Collection;
import java.util.Optional;

@Service
public class CommitteeServiceImpl implements CommitteeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommitteeServiceImpl.class);
    private final CommitteeRepository committeeRepository;

    @Autowired
    public CommitteeServiceImpl(CommitteeRepository committeeRepository) {
        this.committeeRepository = committeeRepository;
    }

    @Override
    public Optional<Committee> getCommitteeById(Long id) {
        LOGGER.debug("Getting committee={}", id);
        return Optional.ofNullable(committeeRepository.findOne(id));
    }

    @Override
    public Optional<Committee> getCommitteeByFullName(String fullName) {
        LOGGER.debug("Getting commitee={}", fullName);
        return committeeRepository.findOneByFullName(fullName);
    }

    @Override
    public Optional<Committee> getCommitteeByShortName(String shortName) {
        LOGGER.debug("Getting commitee={}", shortName);
        return committeeRepository.findOneByShortName(shortName);
    }

    @Override
    public Collection<Committee> getAllCommittees() {
        LOGGER.debug("Getting all committees");
        return committeeRepository.findAll(new Sort("id"));
    }

    @Override
    public Committee create(CommitteeCreateForm form) {
        Committee committee = new Committee();
        committee.setFullName(form.getFullName());
        committee.setShortName(form.getShortName());
        LOGGER.debug("Saving committee={}", committee);
        return committeeRepository.save(committee);
    }

    @Override
    public void delete(Long id) {
        LOGGER.debug("Deleting comission={}", id);
        committeeRepository.delete(id);
        return;
    }
}
