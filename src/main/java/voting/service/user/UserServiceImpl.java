package voting.service.user;


import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import voting.domain.Comission;
import voting.domain.Election;
import voting.domain.User;
import voting.domain.forms.UserCreateForm;
import voting.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getUserById(long id) {
        LOGGER.debug("Getting user={}", id);
        return Optional.ofNullable(userRepository.findOne(id));
    }

    @Override
    public Optional<User> getUserByLogin(String login) {
        LOGGER.debug("Getting user by login={}", login);
        return userRepository.findOneByLogin(login);
    }

    @Override
    public Collection<User> getAllUsers() {
        LOGGER.debug("Getting all users");
        return userRepository.findAll(new Sort("login"));
    }

    @Override
    public Collection<User> getUsersByComission(Comission comission) {
        LOGGER.debug("Getting users by comission={}", comission);
        return userRepository.findByComission(comission);
    }

    @Override
    public User create(UserCreateForm form) {
        LOGGER.debug("Creating user", form);
        User user = new User();
        user.setLogin(form.getLogin());
        user.setPasswordHash(new BCryptPasswordEncoder().encode(form.getPassword()));
        user.setRole(form.getRole());
        user.setComission(form.getComission());
        return userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        LOGGER.debug("Deleting user", id);
        userRepository.delete(id);
        return;
    }

    @Override
    @Transactional
    public void setUserCurrentElection(User user, Election election) {
        LOGGER.debug("Setting current election={} for user={}", election, user);
        user.setElection(election);
        userRepository.setUserCurrentElection(user, election);
    }

}