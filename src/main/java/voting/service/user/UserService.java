package voting.service.user;

import java.util.Collection;
import java.util.Optional;

import voting.domain.Comission;
import voting.domain.Election;
import voting.domain.User;
import voting.domain.forms.UserCreateForm;

public interface UserService {

    Optional<User> getUserById(long id);

    Optional<User> getUserByLogin(String login);

    Collection<User> getAllUsers();

    Collection<User> getUsersByComission(Comission comission);

    User create(UserCreateForm form);

    void delete(Long id);

    void setUserCurrentElection(User user, Election election);

}
