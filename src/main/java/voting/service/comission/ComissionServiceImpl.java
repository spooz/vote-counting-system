package voting.service.comission;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import voting.domain.Comission;
import voting.domain.ComissionType;
import voting.domain.forms.ComissionCreateForm;
import voting.repository.ComissionRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ComissionServiceImpl implements ComissionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComissionServiceImpl.class);
    private final ComissionRepository comissionRepository;

    @Autowired
    public ComissionServiceImpl(ComissionRepository comissionRepository) {
        this.comissionRepository = comissionRepository;
    }

    @Override
    public Optional<Comission> getComissionById(Long id) {
        LOGGER.debug("Getting comission={}", id);
        return Optional.ofNullable(comissionRepository.findOne(id));
    }

    @Override
    public Optional<Comission> getComissionByCode(Long code) {
        LOGGER.debug("Getting comission={}", code);
        return comissionRepository.findOneByCode(code);
    }

    @Override
    public Collection<Comission> getAllComissions() {
        LOGGER.debug("Getting all comission");
        return comissionRepository.findAll(new Sort("id"));
    }

    @Override
    public Collection<Comission> getComissionsByParentComission(Comission parent) {
        LOGGER.debug("Getting comissions by parent comission={}", parent.getId());
        return comissionRepository.findAllByParentComission(parent);
    }

    @Override
    public List<Comission> findByComissionType(ComissionType comissionType) {
        LOGGER.debug("Getting comissions by comission type={}", comissionType);
        return comissionRepository.findByComissionType(comissionType);
    }

    @Override
    public Comission create(ComissionCreateForm form) {
        Comission comission = new Comission();
        comission.setCode(form.getCode());
        comission.setAddress(form.getAddress());
        comission.setComissionType(form.getComissionType());
        comission.setParentComission(form.getParentComission());
        LOGGER.debug("Creating commison={}", comission);
        return comissionRepository.save(comission);
    }

    @Override
    public void delete(Long id) {
        LOGGER.debug("Deleting comission={}", id);
        comissionRepository.delete(id);
        return;
    }

    @Override
    public Comission save(Comission comission) {
        LOGGER.debug("Updating comission={}");
        return comissionRepository.save(comission);
    }

    @Override
    public int getCountOfSubordinateOBKWComissions(Comission comission) {
        if(comission.getComissionType().equals(ComissionType.PKW))
            return this.findByComissionType(ComissionType.ObKW).size(); //TODO OPTYMALIZACJA?
        return this.getComissionsByParentComission(comission).size();
    }
}
