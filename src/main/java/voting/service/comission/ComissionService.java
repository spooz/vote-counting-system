package voting.service.comission;

import voting.domain.Comission;
import voting.domain.ComissionType;
import voting.domain.forms.ComissionCreateForm;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ComissionService {

    Optional<Comission> getComissionById(Long id);

    Optional<Comission> getComissionByCode(Long code);

    Collection<Comission> getAllComissions();

    Collection<Comission> getComissionsByParentComission(Comission parent);

    List<Comission> findByComissionType(ComissionType comissionType);

    Comission create(ComissionCreateForm form);

    void delete(Long id);

    Comission save(Comission comission);

    int getCountOfSubordinateOBKWComissions(Comission comission);
}
