package voting.service.election;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import voting.domain.Election;
import voting.repository.ElectionRepository;

import java.util.Collection;
import java.util.List;

/**
 * Created by Bartosz on 2016-03-23.
 */
@Service
public class ElectionServiceImpl implements ElectionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElectionServiceImpl.class);

    @Autowired
    private ElectionRepository electionRepository;

    @Override
    public List<Election> getAll() {
        LOGGER.debug("Getting all users");
        return electionRepository.findAll();
    }

    @Override
    @Transactional
    public Election findById(Long id) {
        LOGGER.debug("Getting user={}", id);
        return electionRepository.findById(id);
    }

    @Override
    @Transactional
    public void save(Election election) {
        LOGGER.debug("Saving election={}", election);
        electionRepository.save(election);
    }

    @Override
    @Transactional
    public List<Election> findByIsActive(boolean isActive) {
        LOGGER.debug("Finding elections by isActive={}", isActive);
        return electionRepository.findByIsActive(isActive);
    }
}
