package voting.service.election;

import voting.domain.Election;

import java.util.Collection;
import java.util.List;

/**
 * Created by Bartosz on 2016-03-23.
 */
public interface ElectionService {

    List<Election> getAll();
    void save(Election election);
    Election findById(Long id);
    List<Election> findByIsActive(boolean isActive);
}
