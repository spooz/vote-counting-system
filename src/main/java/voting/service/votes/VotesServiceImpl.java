package voting.service.votes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import voting.domain.*;
import voting.repository.VotesRepository;
import voting.service.CreatePDF;
import voting.service.comission.ComissionService;
import voting.service.committee.CommitteeService;
import voting.service.currentuser.CurrentUserDetailsService;
import voting.service.election.ElectionService;
import voting.service.user.UserServiceImpl;
import voting.service.votingList.VotingListService;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.List;
import java.util.Objects;

/**
 * Created by KOZAK on 2016-05-09.
 */
@Service
public class VotesServiceImpl implements VotesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VotesServiceImpl.class);

    @Autowired
    private VotesRepository votesRepository;

    @Autowired
    private ComissionService comissionService;

    @Autowired
    private ElectionService electionService;

    @Autowired
    private VotingListService votingListService;

    @Autowired
    private CurrentUserDetailsService currentUserDetailsService;

    public static final String REPORTS_FILE_PATH = "C:/reports/";


    @Override
    public Votes findById(Long id) {
        LOGGER.debug("Getting votes={}", id);
        return votesRepository.findById(id);
    }

    @Override
    public List<Votes> findByComission(Comission comission) {
        LOGGER.debug("Getting votes={}", comission);
        return votesRepository.findByComission(comission);
    }

    @Override
    public List<Votes> findByCandidate(Candidate candidate) {
        LOGGER.debug("Getting votes={}", candidate);
        return votesRepository.findByCandidate(candidate);
    }

    @Override
    public Votes findByComissionAndCandidate(Comission comission, Candidate candidate) {
        LOGGER.debug("Getting votes={}, {}", comission, candidate);
        return votesRepository.findByComissionAndCandidate(comission, candidate);
    }

    @Override
    @Transactional
    public void save(Votes votes) {
        LOGGER.debug("Saving votes={}", votes);
        votesRepository.save(votes);
    }

    @Override
    public Integer getVotesCountForOKW(Long comissionId) {
       Integer votes =  votesRepository.getVotesCountForOKW(comissionId);
        return votes == null ? 1 : votes;
    }

    @Override
    public Integer getVotesCountForPKW(Long comissionId) {
        Integer votes =  votesRepository.getVotesCountForPKW(comissionId);
        return votes == null ? 1 : votes;
    }





  //  @PostConstruct
    @Scheduled(cron = "* */30 * * * *")
    private void createReports() {

        final String temperotyFilePath = REPORTS_FILE_PATH;
        String fileName;
        List<Election> elections = electionService.findByIsActive(true);


        for(Election election : elections) {
        //pkw
            List<Comission> commissions = comissionService.findByComissionType(ComissionType.PKW);
            for(Comission comission : commissions) {
                List<VotingList> votingLists = null;
                if (election.getElectionType().equals(ElectionType.PRESIDENTIAL)){
                    votingLists = votingListService.findByComissionAndElection(comission, election);
                }

                else if (election.getElectionType().equals(ElectionType.DISTRICT)) {
                    votingLists = new LinkedList<>();
                    for(Comission com : comissionService.getComissionsByParentComission(comission))
                        votingLists.addAll(votingListService.findByComissionAndElection(com, election));
                }


                List<VotingListPDFView> views = new LinkedList<>();
                for(VotingList list : votingLists) {
                    Map<Candidate, Integer> candidatesMap = new HashMap<>();
                    for(Candidate candidate : list.getCandidates()) {
                        Integer amount = this.getVotesResultForCandidateByComission(candidate, comission).getVotes();
                        candidatesMap.put(candidate, amount);
                    }
                    VotingListPDFView view = new VotingListPDFView(comission, list, candidatesMap, votesRepository.getVotesCountForVotingList(list.getId()));
                    views.add(view);
                }

                try {
                    fileName = buildReportFilePath(election.getId(), comission.getComissionType(), comission.getCode());
                    CreatePDF.createPDF(temperotyFilePath+"\\"+fileName, views);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }


            }

            //okw
            commissions = comissionService.findByComissionType(ComissionType.OKW);
            for(Comission comission : commissions) {
                Comission currentComission = comission;
                List<VotingList> votingLists = null;
              if (election.getElectionType().equals(ElectionType.PRESIDENTIAL))
                    comission = comission.getParentComission();



                votingLists = votingListService.findByComissionAndElection(comission, election);


                List<VotingListPDFView> views = new LinkedList<>();
                for(VotingList list : votingLists) {
                    Map<Candidate, Integer> candidatesMap = new HashMap<>();
                    for(Candidate candidate : list.getCandidates()) {
                        Integer amount = this.getVotesResultForCandidateByComission(candidate, currentComission).getVotes();
                        candidatesMap.put(candidate, amount);
                    }
                    VotingListPDFView view = new VotingListPDFView(comission, list, candidatesMap, votesRepository.getVotesCountForVotingList(list.getId()));
                    views.add(view);
                }

                try {
                    fileName = buildReportFilePath(election.getId(), currentComission.getComissionType(), currentComission.getCode());
                    CreatePDF.createPDF(temperotyFilePath+"\\"+fileName, views);

                } catch (Exception e1) {
                    e1.printStackTrace();
                }


            }




        }


    }

    public static String buildReportFilePath(Long electionId, ComissionType comissionType, Long comissionCode ) {
        return electionId + "_" + comissionType + comissionCode + "_report.pdf";
    }

    @Override
    public VotesResult getVotesResultForCandidateByComission(Candidate candidate, Comission comission) {
        Integer count = null;
        Integer comissions = null;
        if(comission.getComissionType() == ComissionType.OKW) {
            if(Objects.equals(candidate.getVotingList().getComission().getId(), comission.getId())) {
                count = votesRepository.getVotesCountForCandidate(candidate.getId());
                comissions = votesRepository.getComissionsCountForCandidate(candidate.getId());
            } else {
                count = votesRepository.getVotesCountForCandidateFromSubordinates(candidate.getId(), comission.getId());
                comissions = votesRepository.getComissionsCountForCandidateFromSubordinates(candidate.getId(), comission.getId());
            }
        } else if(comission.getComissionType() == ComissionType.PKW) {
                count = votesRepository.getVotesCountForCandidate(candidate.getId());
                comissions = votesRepository.getComissionsCountForCandidate(candidate.getId());
        }

        if(count == null)
            count = 0;
        if(comissions == null)
            comissions = 0;
        return new VotesResult(candidate, count, comissions);
    }

    @Override
    public List<VotingListCandidatesView> getVotingListCandidatesViews() {
        Election election = currentUserDetailsService.getCurrentUser().getUser().getElection();


        Comission comission = null;
        Comission myComission = currentUserDetailsService.getCurrentUser().getUser().getComission();
        List<VotingList> votingLists = null;

        if(currentUserDetailsService.getCurrentUser().getRole() == Role.OKW) {
            if (election.getElectionType().equals(ElectionType.PRESIDENTIAL)) {
                comission = currentUserDetailsService.getCurrentUser().getUser().getComission().getParentComission();
            } else if (election.getElectionType().equals(ElectionType.DISTRICT)) {
                comission = currentUserDetailsService.getCurrentUser().getUser().getComission();
            }
            votingLists = votingListService.findByComissionAndElection(comission, election);
        } else {
            if (election.getElectionType().equals(ElectionType.PRESIDENTIAL))
                votingLists = votingListService.findByComissionAndElection(myComission, election);
            else if (election.getElectionType().equals(ElectionType.DISTRICT)) {
                votingLists = new ArrayList<VotingList>();
                for(Comission com : comissionService.getComissionsByParentComission(myComission))
                    votingLists.addAll(votingListService.findByComissionAndElection(com, election));
            }
        }

        List<VotingListCandidatesView> views = new LinkedList<>();

        for(VotingList list : votingLists) {
            List<VotesResult> votesResults = new ArrayList<>();
            for(Candidate candidate : list.getCandidates()) {
                votesResults.add(this.getVotesResultForCandidateByComission(candidate, myComission));
            }
            int allComissionsCount;
            if(myComission.getComissionType().equals(ComissionType.PKW))
                allComissionsCount = comissionService.getCountOfSubordinateOBKWComissions(list.getComission());
            else
                allComissionsCount = comissionService.getCountOfSubordinateOBKWComissions(myComission);
            VotingListCandidatesView view = new VotingListCandidatesView(list, votesResults, allComissionsCount);
            views.add(view);
        }

        return views;
    }
}
