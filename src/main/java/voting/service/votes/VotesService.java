package voting.service.votes;

import voting.domain.*;

import java.util.List;

/**
 * Created by KOZAK on 2016-05-09.
 */
public interface VotesService {
    Votes findById(Long id);
    List<Votes> findByComission(Comission comission);
    List<Votes> findByCandidate(Candidate candidate);
    Votes findByComissionAndCandidate(Comission comission, Candidate candidate);
    void save(Votes votes);
    Integer getVotesCountForOKW(Long comissionId);
    Integer getVotesCountForPKW(Long comissionId);
    VotesResult getVotesResultForCandidateByComission(Candidate candidate, Comission comission);
    List<VotingListCandidatesView> getVotingListCandidatesViews();

}
