package voting.service.candidate;

import org.springframework.stereotype.Service;
import voting.domain.Candidate;
import voting.domain.VotingList;

import java.util.Collection;

/**
 * Created by Bartosz on 2016-04-17.
 */
@Service
public interface CandidateService {

    Candidate findById(Long id);
    void save(Candidate candidate);
    Collection<Candidate> findByVotingList(VotingList votingList);

    void delete(Long id);
}
