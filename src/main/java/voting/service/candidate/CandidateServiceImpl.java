package voting.service.candidate;

import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import voting.domain.Candidate;
import voting.domain.Votes;
import voting.domain.VotingList;
import voting.repository.CandidateRepository;
import voting.service.comission.ComissionServiceImpl;
import voting.service.votes.VotesService;

import java.util.Collection;


/**
 * Created by Bartosz on 2016-04-17.
 */
@Service
public class CandidateServiceImpl implements CandidateService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CandidateServiceImpl.class);

    @Autowired
    private CandidateRepository candidateRepository;
    private VotesService votesService;

    @Override
    public Candidate findById(Long id) {
        LOGGER.debug("Getting candidate by id ={}", id);
        return candidateRepository.findById(id);
    }

    @Override
    @Transactional
    public void save(Candidate candidate) {
        LOGGER.debug("Saving candidate={}", candidate);
        candidateRepository.save(candidate);
        Votes votes = new Votes();
        votes.setCandidate(candidate);
        votes.setComission(candidate.getVotingList().getComission());
    }

    @Override
    public Collection<Candidate> findByVotingList(VotingList votingList) {
        return findByVotingList(votingList);
    }

    @Override
    public void delete(Long id) {
        LOGGER.debug("Deleting comission={}", id);
        candidateRepository.delete(id);
        return;
    }
}
