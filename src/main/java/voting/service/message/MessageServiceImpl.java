package voting.service.message;

import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import voting.comm.Message;
import voting.repository.MessageRepository;

import java.util.Collection;
import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);
    private final MessageRepository messageRepository;

    @Autowired
    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Optional<Message> getMessageById(Long id) {
        LOGGER.debug("Getting message with id ={}", id);
        return messageRepository.findById(id);
    }

    @Override
    public Collection<Message> getMessageByReceiverId(Long id) {
        LOGGER.debug("Getting messages for receiver id ={}", id);
        return messageRepository.findByReceiverId(id);
    }

    @Override
    public Collection<Message> getMessageBySenderId(Long id) {
        LOGGER.debug("Getting messages for sender id ={}", id);
        return messageRepository.findBySenderId(id);
    }

    @Override
    public Collection<Message> getMessageByReceiverIdAndSenderId(Long receiverId, Long senderId) {
        LOGGER.debug("Getting messages for receiver id = {} to sender id ={}", receiverId, senderId);
        Collection<Message> receiverMsgs = messageRepository.findByReceiverId(receiverId);
        receiverMsgs.retainAll(messageRepository.findBySenderId(senderId));
        return receiverMsgs;
    }

    @Override
    public Collection<Message> getUnreadMessage(Long id) {
        LOGGER.debug("Getting unread messages for receiver id = {}", id);
        Collection<Message> collection = messageRepository.findByIsRead(false);
        collection.retainAll(messageRepository.findByReceiverId(id));
        return collection;
    }

    @Override
    public Message create(Message msg) {
        LOGGER.debug("Creating message={}", msg);
        return messageRepository.save(msg);
    }

    @Override
    public void update(Message msg) {
        LOGGER.debug("Updating message={}", msg);
        Optional<Message> old = getMessageById(msg.getId());
        if (old.isPresent()) {
            old.get().setRead(true);
            messageRepository.save(old.get());
        }
    }
}
