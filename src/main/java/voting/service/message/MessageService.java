package voting.service.message;

import voting.comm.Message;

import java.util.Collection;
import java.util.Optional;

public interface MessageService {
    Optional<Message> getMessageById(Long id);
    Collection<Message> getMessageByReceiverId(Long id);
    Collection<Message> getMessageBySenderId(Long id);
    Collection<Message> getMessageByReceiverIdAndSenderId(Long receiverId, Long senderId);
    Collection<Message> getUnreadMessage(Long id);
    Message create(Message msg);
    void update(Message msg);
}
