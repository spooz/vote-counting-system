package voting.service.votingList;

import voting.domain.Comission;
import voting.domain.Election;
import voting.domain.VotingList;
import voting.domain.forms.VotingListForm;

import java.util.List;

/**
 * Created by Bartosz on 2016-04-03.
 */
public interface VotingListService {

    VotingList findById(Long id);
    List<VotingList> findByComissionAndElection(Comission comission, Election election);
    void create(VotingListForm form);
}
