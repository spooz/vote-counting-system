package voting.service.votingList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import voting.domain.Comission;
import voting.domain.Election;
import voting.domain.VotingList;
import voting.domain.forms.VotingListForm;
import voting.repository.VotingListRepository;
import voting.service.user.UserServiceImpl;

import java.util.List;

/**
 * Created by Bartosz on 2016-04-03.
 */
@Service
public class VotingListServiceImpl implements VotingListService{

    private static final Logger LOGGER = LoggerFactory.getLogger(VotingListServiceImpl.class);

    @Autowired
    private VotingListRepository votingListRepository;

    @Override
    public List<VotingList> findByComissionAndElection(Comission comission, Election election) {
        LOGGER.debug("Getting voting lists by commission={} and election={}", comission, election);
        return votingListRepository.findByComissionAndElection(comission, election);
    }

    @Transactional
    @Override
    public void create(VotingListForm form) {
        VotingList list = new VotingList();
        list.setCommittee(form.getCommittee());
        list.setComission(form.getComission());
        list.setListNumber(Long.parseLong(form.getListNumber()));
        list.setElection(form.getElection());
        LOGGER.debug("Creating voting list={}", list);
        votingListRepository.save(list);
    }

    @Override
    public VotingList findById(Long id) {
        LOGGER.debug("Getting voting list={}", id);
        return votingListRepository.findOne(id);
    }
}
