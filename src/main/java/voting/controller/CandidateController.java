package voting.controller;

import org.omg.CORBA.Current;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import voting.domain.Candidate;
import voting.domain.CurrentUser;
import voting.domain.VotingList;
import voting.service.candidate.CandidateService;
import voting.service.currentuser.CurrentUserServiceImpl;
import voting.service.votes.VotesService;

import javax.validation.Valid;

/**
 * Created by Bartosz on 2016-04-17.
 */

@Controller
@PreAuthorize("isAuthenticated()")
public class CandidateController {

    @Autowired
    private CandidateService candidateService;
    @Autowired
    private CurrentUserServiceImpl currentUserServiceImpl;

    @RequestMapping(value = "/candidate/{candidate}", method = RequestMethod.GET)
    public String getCandidate(@PathVariable Candidate candidate, CurrentUser currentUser, Model model) {
        model.addAttribute("candidate", candidate);
        model.addAttribute("accessible", currentUserServiceImpl.canAccessCandidate(currentUser, candidate));
        return "candidate";
    }

    @PreAuthorize("@currentUserServiceImpl.canCreateCandidate(principal, #votingList)")
    @RequestMapping(value = "/list/{votingList}/candidate/add", method = RequestMethod.GET)
    public String createCandidate(@PathVariable VotingList votingList, Model model) {
        model.addAttribute("candidate", new Candidate());
        model.addAttribute("votingList", votingList);
        return "candidate_create";
    }

    @PreAuthorize("@currentUserServiceImpl.canCreateCandidate(principal, #votingList)")
    @RequestMapping(value = "/list/{votingList}/candidate/add", method = RequestMethod.POST)
    public String createCandidatePost(@PathVariable VotingList votingList, @ModelAttribute @Valid Candidate candidate) {
        candidate.setVotingList(votingList);
        candidateService.save(candidate);
        Long listId = votingList.getId();
        return "redirect:/votinglist/ " + listId;
    }


    @PreAuthorize("@currentUserServiceImpl.canAccessCandidate(principal, #candidate)")
    @RequestMapping(value = "/candidate/{candidate}", method = RequestMethod.DELETE)
    @ResponseBody
    public Long handleCandidateDelete(@PathVariable Candidate candidate) {
        Long candidateId = candidate.getId();
        Long votingListId = candidate.getVotingList().getId();
        //LOGGER.debug("Deleting user");
        candidateService.delete(candidateId);
        return votingListId;
    }

    @PreAuthorize("@currentUserServiceImpl.canCreateCandidate(principal, #id)")
    @RequestMapping(value = "/candidate/{id}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void handleCandidateUpdate(@PathVariable Long id, @RequestParam("name") String name, @RequestParam("value") String value) {
        Candidate candidate = null;
        try {
            candidate = candidateService.findById(id);
        }catch(Exception e) {}

        if(name.equals("firstName")) {
            candidate.setFirstName(value);
        } else if(name.equals("lastName")) {
            candidate.setLastName(value);
        }
        candidateService.save(candidate);
        return;
    }

}
