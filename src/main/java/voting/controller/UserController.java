package voting.controller;

import java.beans.PropertyEditorSupport;
import java.util.Collection;
import java.util.HashMap;
import java.util.NoSuchElementException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import voting.domain.Comission;
import voting.domain.Role;
import voting.domain.forms.UserCreateForm;
import voting.domain.validator.UserCreateFormValidator;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;
import voting.service.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@Controller
public class UserController {
    private final UserService userService;
    private final UserCreateFormValidator userCreateFormValidator;
    private final ComissionService comissionService;
    private final CurrentUserDetailsService currentUserDetailsService;
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService, UserCreateFormValidator userCreateFormValidator,
                          ComissionService comissionService, CurrentUserDetailsService currentUserDetailsService) {
        this.userService = userService;
        this.userCreateFormValidator = userCreateFormValidator;
        this.comissionService = comissionService;
        this.currentUserDetailsService = currentUserDetailsService;
    }

    @InitBinder("form")
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Comission.class, "comission", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                Comission comission = null;
                try {
                    comission = comissionService.getComissionById(Long.parseLong(text)).get();
                } catch(Exception e){}
                setValue(comission);
            }
        });
        binder.addValidators(userCreateFormValidator);
    }

    @PreAuthorize("@currentUserServiceImpl.canAccessUser(principal, #id)")
    @RequestMapping("/user/{id}")
    public ModelAndView getUserPage(@PathVariable Long id) {
        return new ModelAndView("user", "user", userService.getUserById(id)
                .orElseThrow(() -> new NoSuchElementException(String.format("User=%s not found", id))));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PKW','OKW')")
    @RequestMapping(value = "/user/create", method = RequestMethod.GET)
    public ModelAndView getUserCreatePage() {
            ModelAndView modelAndView;
        if( currentUserDetailsService.getCurrentUser().getRole() == Role.ADMIN ) {
            modelAndView = new ModelAndView("admin_user_create");
            modelAndView.addObject("form", new UserCreateForm());
            modelAndView.addObject("comissions", comissionService.getAllComissions());
        } else {
            modelAndView = new ModelAndView("user_create");
            modelAndView.addObject("form", new UserCreateForm());
            modelAndView.addObject("comissions", comissionService.getComissionsByParentComission(
                    currentUserDetailsService.getCurrentUser().getUser().getComission()) );
        }
        return modelAndView;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PKW','OKW')")
    @RequestMapping(value = "/user/create", method = RequestMethod.POST)
    public String handleUserCreateForm(@Valid @ModelAttribute("form") UserCreateForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            if( currentUserDetailsService.getCurrentUser().getRole() == Role.ADMIN ) {
                return "admin_user_create";
            }
            return "user_create";
        }
        try {
            LOGGER.debug("Creating user");
            userService.create(form);
        } catch (DataIntegrityViolationException e) {
            LOGGER.debug("Creating user - error login exists");
            bindingResult.reject("login.exists", "Login already exists");
            if( currentUserDetailsService.getCurrentUser().getRole() == Role.ADMIN ) {
                return "admin_user_create";
            }
            return "user_create";
        }
        return "redirect:/users";
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PKW','OKW')")
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void handleUserDelete(@PathVariable Long id) {
        LOGGER.debug("Deleting user");
        userService.delete(id);
    }
}