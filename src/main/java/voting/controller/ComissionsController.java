package voting.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import voting.domain.Role;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;

@Controller
public class ComissionsController {
    private final ComissionService comissionService;
    private final CurrentUserDetailsService currentUserDetailsService;

    @Autowired
    public ComissionsController(ComissionService comissionService, CurrentUserDetailsService currentUserDetailsService) {
        this.comissionService = comissionService;
        this.currentUserDetailsService = currentUserDetailsService;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PKW','OKW')")
    @RequestMapping("/comissions")
    public ModelAndView getComissionsPage() {
        if( currentUserDetailsService.getCurrentUser().getRole() == Role.ADMIN ) {
            return new ModelAndView("comissions", "comissions", comissionService.getAllComissions());
        }
        return new ModelAndView("comissions", "comissions",comissionService.getComissionsByParentComission(
                currentUserDetailsService.getCurrentUser().getUser().getComission()) );
    }
}
