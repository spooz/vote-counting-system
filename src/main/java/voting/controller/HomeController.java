package voting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import voting.domain.CurrentUser;
import voting.domain.Election;
import voting.service.election.ElectionService;


@Controller
public class HomeController {

    @Autowired
    private ElectionService electionService;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping("/")
    public String getHomePage(CurrentUser currentUser, Model model) {
        model.addAttribute("elections", electionService.findByIsActive(true));
        model.addAttribute("currentElection", new Election());
        return "home";
    }

}