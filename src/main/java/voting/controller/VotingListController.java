package voting.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import voting.domain.*;
import voting.domain.forms.VotingListForm;
import voting.domain.validator.VotingListFormValidator;
import voting.service.comission.ComissionService;
import voting.service.committee.CommitteeService;
import voting.service.election.ElectionService;
import voting.service.votingList.VotingListService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Bartosz on 2016-03-20.
 */

@Controller
public class VotingListController {

    @Autowired
    private VotingListService votingListService;

    @Autowired
    private ElectionService electionService;

    @Autowired
    private CommitteeService committeeService;

    @Autowired
    private ComissionService comissionService;

    @Autowired
    private VotingListFormValidator votingListFormValidator;

    private static final Logger LOGGER = LoggerFactory.getLogger(VotingListController.class);

    @InitBinder("form")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(votingListFormValidator);
    }

    @PreAuthorize("@currentUserServiceImpl.canAccessVotingList(principal, #votingList)")
    @RequestMapping(value = "/votinglist/{votingList}", method = RequestMethod.GET)
    public String getVotingList(@PathVariable VotingList votingList, CurrentUser currentUser, Model model) {
        model.addAttribute("votingList", votingList);
        return "votinglist";
    }

    @PreAuthorize("hasAnyAuthority('OKW','PKW')")
    @RequestMapping(value = "/votinglists/{comission}", method = RequestMethod.GET)
    public String getAllVotingListsByComission(@PathVariable Comission comission, CurrentUser currentUser, Model model) {
        List<VotingList> lists = votingListService.findByComissionAndElection(comission, currentUser.getUser().getElection()); // pobieramy listy po wybranych przez usera wyborach
        model.addAttribute("votingLists", lists);
        model.addAttribute("elections", electionService.findByIsActive(true));
        model.addAttribute("comission", comission);
        return "votinglists_all_by_comission";
    }

    @PreAuthorize("hasAnyAuthority('PKW','OKW','OBWKW')")
    @RequestMapping(value = "/votinglists", method = RequestMethod.GET)
    public String getAllVotingLists(CurrentUser currentUser, Model model) {
        List<VotingList> lists;
        if(currentUser.getUser().getElection() == null) {
            lists =  new ArrayList<VotingList>();
        } else if((currentUser.getRole() == Role.PKW && currentUser.getUser().getElection().getElectionType() == ElectionType.PRESIDENTIAL) || (currentUser.getRole() == Role.OKW && currentUser.getUser().getElection().getElectionType() == ElectionType.DISTRICT)) {
            lists = votingListService.findByComissionAndElection(currentUser.getUser().getComission(), currentUser.getUser().getElection());
        } else if(currentUser.getRole() == Role.PKW) {
            lists = new ArrayList<VotingList>();
            for(Comission comission : comissionService.getComissionsByParentComission(currentUser.getUser().getComission())) {
                lists.addAll(votingListService.findByComissionAndElection(comission, currentUser.getUser().getElection()));
            }
            model.addAttribute("votingLists", lists);
            model.addAttribute("elections", electionService.findByIsActive(true));
            return "votinglists_all_pkw";
        } else if((currentUser.getRole() == Role.OKW && currentUser.getUser().getElection().getElectionType() == ElectionType.PRESIDENTIAL)
                || (currentUser.getRole() == Role.OBWKW && currentUser.getUser().getElection().getElectionType() == ElectionType.DISTRICT))
            lists = votingListService.findByComissionAndElection(currentUser.getUser().getComission().getParentComission(), currentUser.getUser().getElection());
        else
            lists = votingListService.findByComissionAndElection(currentUser.getUser().getComission().getParentComission().getParentComission(), currentUser.getUser().getElection());
        model.addAttribute("votingLists", lists);
        model.addAttribute("elections", electionService.findByIsActive(true));
        return "votinglists_all";
    }

    @PreAuthorize("hasAnyAuthority('PKW','OKW')")
    @RequestMapping(value ="/votinglists/add", method = RequestMethod.GET)
    public String createVotingList(CurrentUser currentUser, Model model) {
        List<Election> elections = electionService.getAll();
        Collection<Committee> committees = committeeService.getAllCommittees();
        model.addAttribute("elections", elections);
        model.addAttribute("committees", committees);
        model.addAttribute("form", new VotingListForm());
        return "votinglist_create";
    }

    @PreAuthorize("hasAnyAuthority('PKW','OKW')")
    @RequestMapping(value = "/votinglists/add", method = RequestMethod.POST)
    public String createVotingListPost(@Valid @ModelAttribute("form") VotingListForm votingListForm, BindingResult bindingResult, CurrentUser currentUser) {
        if(bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors().toString());
            return "votinglist_create";
        }
        try {
            LOGGER.debug("Creating voting list");
            votingListService.create(votingListForm);
        } catch (DataIntegrityViolationException e) {
            LOGGER.debug("Creating voting list - error");
            bindingResult.reject("login.exists", "Error while adding voting list");
            return "votinglist_create";
        }
        return "redirect:/votinglists/" + votingListForm.getComission().getId();
    }

}
