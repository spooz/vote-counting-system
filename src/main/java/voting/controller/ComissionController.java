package voting.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import voting.domain.Comission;
import voting.domain.Role;
import voting.domain.forms.ComissionCreateForm;
import voting.domain.forms.UserCreateForm;
import voting.domain.validator.ComissionCreateFormValidator;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.util.NoSuchElementException;
import java.util.Optional;


@Controller
public class ComissionController {
    private final ComissionService comissionService;
    private final ComissionCreateFormValidator comissionCreateFormValidator;
    private final CurrentUserDetailsService currentUserDetailsService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ComissionController.class);

    @Autowired
    public ComissionController(ComissionService comissionService, ComissionCreateFormValidator comissionCreateFormValidator,
                               CurrentUserDetailsService currentUserDetailsService) {
        this.comissionService = comissionService;
        this.comissionCreateFormValidator = comissionCreateFormValidator;
        this.currentUserDetailsService = currentUserDetailsService;
    }

    @InitBinder("form")
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Comission.class, "parentComission", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                System.out.println(text);
                Comission comission = null;
                try {
                    comission = comissionService.getComissionById(Long.parseLong(text)).get();
                }
                catch(Exception e){}
                setValue(comission);
            }
        });
        binder.addValidators(comissionCreateFormValidator);
    }

    @PreAuthorize("@currentUserServiceImpl.canAccessComission(principal, #id)")
    @RequestMapping("/comission/{id}")
    public ModelAndView getComissionPage(@PathVariable Long id) {
        return new ModelAndView("comission", "comission", comissionService.getComissionById(id)
                .orElseThrow(() -> new NoSuchElementException(String.format("Comission=%s not found", id))));
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/comission/create", method = RequestMethod.GET)
    public ModelAndView getComissionCreatePage() {
        ModelAndView modelAndView = new ModelAndView("admin_comission_create");
        modelAndView.addObject("form", new ComissionCreateForm());
        modelAndView.addObject("comissions", comissionService.getAllComissions());
        return modelAndView;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/comission/create", method = RequestMethod.POST)
    public String handleComissionCreateForm(@Valid @ModelAttribute("form") ComissionCreateForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin_comission_create";
        }
        try {
            LOGGER.debug("Creating comission");
            comissionService.create(form);
        } catch (DataIntegrityViolationException e) {
            LOGGER.debug("Creating comission - error code exists");
            bindingResult.reject("code.exists", "Code already exists");
            return "admin_comission_create";
        }
        return "redirect:/comissions";
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PKW','OKW')")
    @RequestMapping(value = "/comission/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void handleComissionDelete(@PathVariable Long id) {
        LOGGER.debug("Deleting commision");
        comissionService.delete(id);
        return;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PKW','OKW')")
    @RequestMapping(value = "/comission/{id}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void handleComissionUpdate(@PathVariable Long id, @RequestParam("name") String name, @RequestParam("value") String value) {
        LOGGER.debug("Updating comission");
        System.out.println(name);
        Comission comission = null;
        try {
            comission = comissionService.getComissionById(id).get();
        }catch(Exception e) {}
        comission.setAddress(value);
        comissionService.save(comission);
        return;
    }

}

