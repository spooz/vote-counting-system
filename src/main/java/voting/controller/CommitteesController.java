package voting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import voting.service.committee.CommitteeService;

@Controller
public class CommitteesController {
    private final CommitteeService committeeService;

    @Autowired
    public CommitteesController(CommitteeService committeeService) {
        this.committeeService = committeeService;
    }

    @PreAuthorize("hasAuthority('PKW')")
    @RequestMapping("/committees")
    public ModelAndView getCommitteesPage() {
        return new ModelAndView("committees", "committees", committeeService.getAllCommittees());
    }
}
