package voting.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import voting.domain.*;
import voting.service.CreatePDF;
import voting.service.candidate.CandidateService;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;
import voting.service.election.ElectionService;
import voting.service.votes.VotesService;
import voting.service.votes.VotesServiceImpl;
import voting.service.votingList.VotingListService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * Created by KOZAK on 2016-05-10.
 */
@Controller
@PreAuthorize("isAuthenticated()")
public class VotesController {
    @Autowired
    private VotesService votesService;
    @Autowired
    private CurrentUserDetailsService currentUserDetailsService;
    @Autowired
    private VotingListService votingListService;
    @Autowired
    private CandidateService candidateService;
    @Autowired
    private ComissionService comissionService;
    @Autowired
    private ElectionService electionService;
    private static final Logger LOGGER = LoggerFactory.getLogger(VotesController.class);
    private Candidate candidate;

    @PreAuthorize("hasAuthority('OBWKW')")
    @RequestMapping(value = "/votes/candidates", method = RequestMethod.GET)
    public String getCandidatesList(Model model) {
        Election election = currentUserDetailsService.getCurrentUser().getUser().getElection();
        Comission comission = null;
        Comission myComission = currentUserDetailsService.getCurrentUser().getUser().getComission();

        if (election == null) {
            model.addAttribute("error", "Before you enter vote scores, you have to choose election");
            model.addAttribute("elections", electionService.findByIsActive(true));
            return "home";
        } else if (election.getElectionType().equals(ElectionType.PRESIDENTIAL)) {
            comission = currentUserDetailsService.getCurrentUser().getUser().getComission().getParentComission().getParentComission();
        } else if (election.getElectionType().equals(ElectionType.DISTRICT)) {
            comission = currentUserDetailsService.getCurrentUser().getUser().getComission().getParentComission();
        }



        List<VotingList> votingLists = votingListService.findByComissionAndElection(comission, election);
        List<VotingListCandidatesView> views = new LinkedList<>();

        for(VotingList list : votingLists) {
            List<VotesResult> votesResults = new ArrayList<>();
            for(Candidate candidate : list.getCandidates()) {
                Votes v = votesService.findByComissionAndCandidate(myComission, candidate);
                Integer amount = -1;
                if(v != null)
                    amount = v.getAmount();
                votesResults.add(new VotesResult(candidate, amount, 0));
            }
            VotingListCandidatesView view = new VotingListCandidatesView(list, votesResults, 0);
            views.add(view);
        }

        model.addAttribute("views", views);
        return "votes_list";
    }

    @PreAuthorize("hasAnyAuthority('PKW','OKW')")
    @RequestMapping(value = "/votes/candidates/show", method = RequestMethod.GET)
    public String getCandidatesListShow(Model model) {
        Election election = currentUserDetailsService.getCurrentUser().getUser().getElection();
        if(election == null) {
            model.addAttribute("error", "Before you can access vote scores, you have to choose election");
            model.addAttribute("elections", electionService.findByIsActive(true));
            return "home";
        }


        model.addAttribute("views", votesService.getVotingListCandidatesViews());
        return "votes_list_show";
    }

    @RequestMapping(value = "/votes/candidates/show/pdf")
    public void getPDF(HttpServletRequest request, HttpServletResponse response) {
        final String temperotyFilePath = VotesServiceImpl.REPORTS_FILE_PATH;
        String fileName = VotesServiceImpl.buildReportFilePath(currentUserDetailsService.getCurrentUser().getUser().getElection().getId(), currentUserDetailsService.getCurrentUser().getUser().getComission().getComissionType(), currentUserDetailsService.getCurrentUser().getUser().getComission().getCode());


        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "attachment; filename="+ fileName);


        try {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos = convertPDFToByteArrayOutputStream(temperotyFilePath+"\\"+fileName);
            LOGGER.debug(temperotyFilePath + " !!!!!!!!!!!!!!!!!");
            OutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    private ByteArrayOutputStream convertPDFToByteArrayOutputStream(String fileName) {

        InputStream inputStream = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {

            inputStream = new FileInputStream(fileName);
            byte[] buffer = new byte[1024];
            baos = new ByteArrayOutputStream();

            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return baos;
    }


    @PreAuthorize("hasAuthority('OBWKW')")
    @RequestMapping(value = "/votes/add", method = RequestMethod.POST)
    public @ResponseBody String addVotes(@RequestParam("value") Integer amount, @RequestParam("candidateId") Long candidateId, @RequestParam("previousValue") Integer previousValue ){
        Comission comission = currentUserDetailsService.getCurrentUser().getUser().getComission();
        Candidate candidate = candidateService.findById(candidateId);

        if(previousValue < 0) {
            Votes votes = new Votes();
            votes.setCandidate(candidate);
            votes.setComission(comission);
            votes.setAmount(amount);
            votesService.save(votes);
        }
        else {

            Votes votes = votesService.findByComissionAndCandidate(comission, candidate);
            votes.setAmount(amount);
            votesService.save(votes);
        }
        String response = "{\"candidateId\":\"" + candidateId + "\",\"previousValue\":\"" + amount + "\"}";
        return response;

    }

    @PreAuthorize("hasAuthority('OBWKW')")
    @RequestMapping(value = "/votes/update", method = RequestMethod.PUT, produces="application/json")
    public @ResponseBody void updateVotes(@RequestParam("value") Integer amount, @RequestParam("candidateId") Long candidateId) {
        Comission comission = currentUserDetailsService.getCurrentUser().getUser().getComission();
        Candidate candidate = candidateService.findById(candidateId);
        Votes votes = votesService.findByComissionAndCandidate(comission, candidate);
        votes.setAmount(amount);
        votesService.save(votes);
        return;
    }
}
