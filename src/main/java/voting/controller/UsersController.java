package voting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.authentication.CachingUserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import voting.domain.Comission;
import voting.domain.Role;
import voting.domain.User;
import voting.domain.forms.UserCreateForm;
import voting.service.comission.ComissionService;
import voting.service.currentuser.CurrentUserDetailsService;
import voting.service.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


@Controller
public class UsersController {

    private final UserService userService;
    private final ComissionService comissionService;
    private final CurrentUserDetailsService currentUserDetailsService;

    @Autowired
    public UsersController(UserService userService, ComissionService comissionService, CurrentUserDetailsService currentUserDetailsService) {
        this.userService = userService;
        this.currentUserDetailsService = currentUserDetailsService;
        this.comissionService = comissionService;
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PKW','OKW')")
    @RequestMapping("/users")
    public ModelAndView getUsersPage() {
        Collection<User> users = new ArrayList<>();
        if( currentUserDetailsService.getCurrentUser().getRole() == Role.ADMIN ) {
            users = userService.getAllUsers();
        }
        else {
            Collection<Comission> commisions = comissionService.getComissionsByParentComission(
                    currentUserDetailsService.getCurrentUser().getUser().getComission());
            for (Comission comission: commisions) {
                users.addAll(comission.getUsers());
            }
        }
        return new ModelAndView("users", "users", users);
    }

}