package voting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import voting.domain.CurrentUser;
import voting.domain.Election;
import voting.service.election.ElectionService;
import voting.service.user.UserService;

/**
 * Created by Bartosz on 2016-03-23.
 */
@PreAuthorize("hasAuthority('PKW')")
@Controller
public class ElectionController {

    @Autowired
    private ElectionService electionService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/election/create", method = RequestMethod.GET)
    public String getElectionCreate(Model model) {
        Election election = new Election();
        model.addAttribute("election", election);
        return "create_election";
    }

    @RequestMapping(value = "/election/create", method = RequestMethod.POST)
    public String createElection(@ModelAttribute Election election, Model model) {
        if(election.getStartDate().after(election.getEndDate())){
            model.addAttribute("error", true);
            model.addAttribute("elecction", election);
            return "create_election";
        }
        electionService.save(election);
        return "redirect:/";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/election/change", method = RequestMethod.POST)
    public String setCurrentElectionForUser(CurrentUser currentUser, @ModelAttribute(value = "election")Election election, @RequestParam("path") String path) {
        if( election.getId() != null )
            userService.setUserCurrentElection(currentUser.getUser(), election);
        return "redirect:" + path;
    }


}
