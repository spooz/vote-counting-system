package voting.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import voting.domain.forms.ComissionCreateForm;
import voting.domain.forms.CommitteeCreateForm;
import voting.domain.validator.CommitteeCreateFormValidator;
import voting.service.committee.CommitteeService;

import javax.validation.Valid;
import java.util.NoSuchElementException;

@Controller
public class CommitteeController {
    private final static Logger LOGGER = LoggerFactory.getLogger(CommitteeController.class);

    private final CommitteeService committeeService;
    private final CommitteeCreateFormValidator committeeCreateFormValidator;

    @Autowired
    public CommitteeController(CommitteeService committeeService, CommitteeCreateFormValidator committeeCreateFormValidator) {
        this.committeeService = committeeService;
        this.committeeCreateFormValidator = committeeCreateFormValidator;
    }

    @InitBinder("form")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(committeeCreateFormValidator);
    }

    @PreAuthorize("hasAuthority('PKW')")
    @RequestMapping(value = "/committee/create", method = RequestMethod.GET)
    public ModelAndView getCommitteCreatePage() {
        ModelAndView modelAndView = new ModelAndView("committee_create");
        modelAndView.addObject("form", new CommitteeCreateForm());
        return modelAndView;
    }

    @PreAuthorize("hasAuthority('PKW')")
    @RequestMapping(value = "/committee/create", method = RequestMethod.POST)
    public String handleCommitteCreateForm(@Valid @ModelAttribute("form") CommitteeCreateForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "committee_create";
        }
        committeeService.create(form);
        return "redirect:/committees";
    }

    @PreAuthorize("hasAuthority('PKW')")
    @RequestMapping("/committee/{id}")
    public ModelAndView getCommitteePage(@PathVariable Long id) {
        return new ModelAndView("committee", "committee", committeeService.getCommitteeById(id)
                .orElseThrow(() -> new NoSuchElementException(String.format("Committee=%s not found", id))));
    }
}
