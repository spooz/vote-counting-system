package voting.comm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import voting.service.currentuser.CurrentUserService;
import voting.service.message.MessageService;
import voting.service.user.UserService;

import java.security.Principal;
import java.util.*;

@Controller
public class MsgController {
    private final UserService userService;
    private final CurrentUserService currentUserService;
    private final MessageService messageService;
    private final SimpMessagingTemplate template;

    @Autowired
    public MsgController(UserService userService, CurrentUserService currentUserService, MessageService messageService,
                         SimpMessagingTemplate template) {
        this.userService = userService;
        this.currentUserService = currentUserService;
        this.messageService = messageService;
        this.template = template;
    }

    @MessageMapping("/chat")
    public void fillUserName(@Payload Message msg, Principal principal) throws Exception {
        msg.setSenderId(userService.getUserByLogin(principal.getName()).get().getId());
        msg.setSenderName((principal.getName()));
        msg.setDate(new Date());
        msg.setReceiverId(userService.getUserByLogin(msg.getReceiverName()).get().getId());
        Message foo = messageService.create(msg);
        this.template.convertAndSend("/user/" + msg.getReceiverName() + "/topic/messages", foo);
        this.template.convertAndSend("/user/" + msg.getSenderName() + "/topic/messages", foo);
    }

    @MessageMapping("/accessibleusers")
    public void fillUserName(String msg, Principal principal) throws Exception {
        List<ShortUser> list = new ArrayList<ShortUser>();
        currentUserService.getAccessableUsers(userService.getUserByLogin(principal.getName()).get()).forEach(user -> {
            if(!Objects.equals(user.getLogin(), principal.getName())) {
                ShortUser shortUser = new ShortUser();
                shortUser.setId(user.getId());
                shortUser.setName(user.getLogin());
                list.add(shortUser);
            }
        });
        this.template.convertAndSend("/user/" + principal.getName() + "/topic/accessibleusers", list);
    }

    @MessageMapping("/openConversation")
    public void passMessages(String name, Principal principal) throws Exception {
        Long receiverId = userService.getUserByLogin(principal.getName()).get().getId();
        Long senderId = userService.getUserByLogin(name).get().getId();
        Collection<Message> messages = messageService.getMessageByReceiverIdAndSenderId(receiverId, senderId);
        messages.addAll(messageService.getMessageByReceiverIdAndSenderId(senderId, receiverId));
        List<Message> list = new ArrayList<>(messages);
        Collections.sort(list);
        this.template.convertAndSend("/user/" + principal.getName() + "/topic/openConversation", list);
    }

    @MessageMapping("/unread")
    public void passUnreadMessages(String msg, Principal principal) throws Exception {
        Collection<Message> collection
                = messageService.getUnreadMessage(userService.getUserByLogin(principal.getName()).get().getId());
        this.template.convertAndSend("/user/" + principal.getName() + "/topic/unread", collection);
    }

    @MessageMapping("/readMessage")
    public void markMessageAsRead(Long id, Principal principal) throws Exception {
        Optional<Message> msg = messageService.getMessageById(id);
        if(principal.getName().equals(msg.get().getReceiverName())) {
            msg.get().setRead(true);
            messageService.update(msg.get());
        }
    }
}