package voting.comm;

/**
 * Created by Mateusz on 20.05.2016.
 */
public class ShortUser {
    private String name;
    private Long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
