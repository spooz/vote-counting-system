package voting.comm;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "message")
public class Message implements Comparable<Message> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;
    @Column(name = "receiver_id")
    private Long receiverId;
    @Column(name = "reciver_name")
    private String receiverName;
    @Column(name = "sender_id")
    private Long senderId;
    @Column(name = "sender_name")
    private String senderName;
    @Column(name = "date")
    private Date date;
    @Column(name = "message")
    private String message;
    @Column(name = "is_read")
    private boolean isRead;

    public Message() {

    }

    public Message(Long receiverId, String receiverName, Long senderId, String senderName, Date date, String message) {
        this.receiverId = receiverId;
        this.receiverName = receiverName;
        this.senderId = senderId;
        this.senderName = senderName;
        this.date = date;
        this.message = message;
    }


    public Long getId() {
        return this.id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", receiverId=" + receiverId +
                ", receiverName='" + receiverName + '\'' +
                ", senderId=" + senderId +
                ", senderName='" + senderName + '\'' +
                ", date=" + date +
                ", message='" + message + '\'' +
                ", isRead=" + isRead +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return id.equals(message.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public int compareTo(Message o) {
        return date.compareTo(o.date);
    }
}
