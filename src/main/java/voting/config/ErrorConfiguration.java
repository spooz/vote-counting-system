package voting.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * Created by Bartosz on 2015-12-13.
 */
@Configuration
class ErrorConfiguration implements EmbeddedServletContainerCustomizer {

    /**
     * Set error pages for specific error response codes
     */
    @Override public void customize( ConfigurableEmbeddedServletContainer container ) {
        container.addErrorPages( new ErrorPage( HttpStatus.NOT_FOUND, "/WEB-INF/errorPage/404.jsp" ) );
        container.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/WEB-INF/errorPage/403.jsp"));
        container.addErrorPages(new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/WEB-INF/errorPage/500.jsp"));
    }

}
