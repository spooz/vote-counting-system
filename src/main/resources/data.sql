-- Komisje
-- Państwowe komisje wyborcze
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (1, 'Polska, Warszawa ul. Wiejska 10', 0, 'PKW', NULL);

-- Okręgowe komisje wyborcze
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (2, 'Legnica', 1, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (3, 'Wałbrzych', 2, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (4, 'Wrocław', 3, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (5, 'Bydgoszcz', 4, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (6, 'Toruń', 5, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (7, 'Lublin', 6, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (8, 'Chełm', 7, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (9, 'Zielona Góra', 8, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (10, 'Łódź', 9, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (11, 'Piotrków Trybunalski', 10, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (12, 'Sieradz', 11, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (13, 'Chrzanów', 12, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (14, 'Kraków', 13, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (15, 'Nowy Sącz', 14, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (16, 'Tarnów', 15, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (17, 'Płock', 16, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (18, 'Radom', 17, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (19, 'Siedlce', 18, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (20, 'Warszawa_1', 19, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (21, 'Warszawa_2', 20, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (22, 'Opole', 21, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (23, 'Krosno', 22, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (24, 'Rzeszów', 23, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (25, 'Białystok', 24, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (26, 'Gdańsk', 25, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (27, 'Gdynia', 26, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (28, 'Bielsko-Biała', 27, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (29, 'Częstochowa', 28, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (30, 'Gliwice', 29, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (31, 'Rybnik', 30, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (32, 'Katowice', 31, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (33, 'Sosnowiec', 32, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (34, 'Kielce', 33, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (35, 'Elbląg', 34, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (36, 'Olsztyn', 35, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (37, 'Kalisz', 36, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (38, 'Konin', 37, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (39, 'Piła', 38, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (40, 'Poznań', 39, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (41, 'Koszalin', 40, 'OKW', 1);
INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
VALUES (42, 'Szczecin', 41, 'OKW', 1);


-- Obwodowe komisje wyborcze
-- Okręg 1
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (43, 'Legnica - Obwód 1', 1, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (44, 'Legnica - Obwód 2', 2, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (45, 'Legnica - Obwód 3', 3, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (46, 'Legnica - Obwód 4', 4, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (47, 'Legnica - Obwód 5', 5, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (48, 'Legnica - Obwód 6', 6, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (49, 'Legnica - Obwód 7', 7, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (50, 'Legnica - Obwód 8', 8, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (51, 'Legnica - Obwód 9', 9, 'ObKW', 2);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (52, 'Legnica - Obwód 10', 10, 'ObKW', 2);
-- Okręg 2
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (53, 'Wałbrzych - Obwód 1', 1, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (54, 'Wałbrzych - Obwód 2', 2, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (55, 'Wałbrzych - Obwód 3', 3, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (56, 'Wałbrzych - Obwód 4', 4, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (57, 'Wałbrzych - Obwód 5', 5, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (58, 'Wałbrzych - Obwód 6', 6, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (59, 'Wałbrzych - Obwód 7', 7, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (60, 'Wałbrzych - Obwód 8', 8, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (61, 'Wałbrzych - Obwód 9', 9, 'ObKW', 3);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (62, 'Wałbrzych - Obwód 10', 10, 'ObKW', 3);
-- Okręg 3
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (63, 'Wrocław - Obwód 1', 1, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (64, 'Wrocław - Obwód 2', 2, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (65, 'Wrocław - Obwód 3', 3, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (66, 'Wrocław - Obwód 4', 4, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (67, 'Wrocław - Obwód 5', 5, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (68, 'Wrocław - Obwód 6', 6, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (69, 'Wrocław - Obwód 7', 7, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (70, 'Wrocław - Obwód 8', 8, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (71, 'Wrocław - Obwód 9', 9, 'ObKW', 4);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (72, 'Wrocław - Obwód 10', 10, 'ObKW', 4);
-- Okręg 4
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (73, 'Bydgoszcz - Obwód 1', 1, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (74, 'Bydgoszcz - Obwód 2', 2, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (75, 'Bydgoszcz - Obwód 3', 3, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (76, 'Bydgoszcz - Obwód 4', 4, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (77, 'Bydgoszcz - Obwód 5', 5, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (78, 'Bydgoszcz - Obwód 6', 6, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (79, 'Bydgoszcz - Obwód 7', 7, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (80, 'Bydgoszcz - Obwód 8', 8, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (81, 'Bydgoszcz - Obwód 9', 9, 'ObKW', 5);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (82, 'Bydgoszcz - Obwód 10', 10, 'ObKW', 5);
-- Okręg 5
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (83, 'Toruń - Obwód 1', 1, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (84, 'Toruń - Obwód 2', 2, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (85, 'Toruń - Obwód 3', 3, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (86, 'Toruń - Obwód 4', 4, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (87, 'Toruń - Obwód 5', 5, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (88, 'Toruń - Obwód 6', 6, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (89, 'Toruń - Obwód 7', 7, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (90, 'Toruń - Obwód 8', 8, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (91, 'Toruń - Obwód 9', 9, 'ObKW', 6);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (92, 'Toruń - Obwód 10', 10, 'ObKW', 6);
-- Okręg 6
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (93, 'Lublin - Obwód 1', 1, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (94, 'Lublin - Obwód 2', 2, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (95, 'Lublin - Obwód 3', 3, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (96, 'Lublin - Obwód 4', 4, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (97, 'Lublin - Obwód 5', 5, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (98, 'Lublin - Obwód 6', 6, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (99, 'Lublin - Obwód 7', 7, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (100, 'Lublin - Obwód 8', 8, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (101, 'Lublin - Obwód 9', 9, 'ObKW', 7);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (102, 'Lublin - Obwód 10', 10, 'ObKW', 7);
-- Okręg 7
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (103, 'Chełm - Obwód 1', 1, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (104, 'Chełm - Obwód 2', 2, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (105, 'Chełm - Obwód 3', 3, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (106, 'Chełm - Obwód 4', 4, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (107, 'Chełm - Obwód 5', 5, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (108, 'Chełm - Obwód 6', 6, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (109, 'Chełm - Obwód 7', 7, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (110, 'Chełm - Obwód 8', 8, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (111, 'Chełm - Obwód 9', 9, 'ObKW', 8);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (112, 'Chełm - Obwód 10', 10, 'ObKW', 8);
-- Okręg 8
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (113, 'Zielona Góra - Obwód 1', 1, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (114, 'Zielona Góra - Obwód 2', 2, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (115, 'Zielona Góra - Obwód 3', 3, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (116, 'Zielona Góra - Obwód 4', 4, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (117, 'Zielona Góra - Obwód 5', 5, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (118, 'Zielona Góra - Obwód 6', 6, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (119, 'Zielona Góra - Obwód 7', 7, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (120, 'Zielona Góra - Obwód 8', 8, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (121, 'Zielona Góra - Obwód 9', 9, 'ObKW', 9);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (122, 'Zielona Góra - Obwód 10', 10, 'ObKW', 9);
-- Okręg 9
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (123, 'Łódź - Obwód 1', 1, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (124, 'Łódź - Obwód 2', 2, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (125, 'Łódź - Obwód 3', 3, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (126, 'Łódź - Obwód 4', 4, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (127, 'Łódź - Obwód 5', 5, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (128, 'Łódź - Obwód 6', 6, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (129, 'Łódź - Obwód 7', 7, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (130, 'Łódź - Obwód 8', 8, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (131, 'Łódź - Obwód 9', 9, 'ObKW', 10);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (132, 'Łódź - Obwód 10', 10, 'ObKW', 10);
-- Okręg 10
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (133, 'Piotrków Trybunalski - Obwód 1', 1, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (134, 'Piotrków Trybunalski - Obwód 2', 2, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (135, 'Piotrków Trybunalski - Obwód 3', 3, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (136, 'Piotrków Trybunalski - Obwód 4', 4, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (137, 'Piotrków Trybunalski - Obwód 5', 5, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (138, 'Piotrków Trybunalski - Obwód 6', 6, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (139, 'Piotrków Trybunalski - Obwód 7', 7, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (140, 'Piotrków Trybunalski - Obwód 8', 8, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (141, 'Piotrków Trybunalski - Obwód 9', 9, 'ObKW', 11);
    INSERT INTO comission (id, address, code, comission_type, parent_comission_id)
    VALUES (142, 'Piotrków Trybunalski - Obwód 10', 10, 'ObKW', 11);


-- Users
-- Admins
INSERT INTO user (login, password_hash, role)
VALUES ('admin', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'ADMIN');

-- PKW
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('pkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'PKW', 1);

-- OKW
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 2);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 3);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 4);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 5);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 6);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 7);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 8);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 9);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 10);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 11);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw11', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 12);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw12', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 13);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw13', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 14);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw14', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 15);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw15', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 16);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw16', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 17);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw17', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 18);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw18', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 19);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw19', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 20);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw20', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 21);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw21', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 22);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw22', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 23);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw23', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 24);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw24', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 25);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw25', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 26);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw26', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 27);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw27', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 28);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw28', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 29);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw29', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 30);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw30', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 31);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw31', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 32);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw32', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 33);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw33', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 34);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw34', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 35);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw35', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 36);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw36', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 37);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw37', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 38);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw38', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 39);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw39', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 40);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw40', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 41);
INSERT INTO user (login, password_hash, role, comission_id)
VALUES ('okw41', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OKW', 42);

-- OBWKW
-- Okręg 1
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 43);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 44);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 45);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 46);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 47);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 48);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 49);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 50);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 51);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw1obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 52);
-- Okręg 2
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 53);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 54);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 55);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 56);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 57);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 58);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 59);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 60);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 61);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw2obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 62);
-- Okręg 3
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 63);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 64);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 65);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 66);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 67);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 68);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 69);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 70);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 71);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw3obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 72);
-- Okręg 4
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 73);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 74);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 75);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 76);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 77);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 78);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 79);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 80);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 81);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw4obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 82);
-- Okręg 5
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 83);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 84);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 85);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 86);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 87);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 88);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 89);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 90);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 91);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw5obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 92);
-- Okręg 6
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 93);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 94);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 95);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 96);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 97);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 98);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 99);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 100);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 101);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw6obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 102);
-- Okręg 7
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 103);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 104);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 105);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 106);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 107);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 108);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 109);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 110);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 111);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw7obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 112);
-- Okręg 8
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 113);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 114);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 115);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 116);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 117);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 118);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 119);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 120);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 121);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw8obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 122);
-- Okręg 9
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 123);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 124);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 125);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 126);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 127);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 128);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 129);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 130);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 131);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw9obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 132);
-- Okręg 10
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw1', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 133);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw2', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 134);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw3', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 135);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw4', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 136);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw5', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 137);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw6', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 138);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw7', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 139);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw8', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 140);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw9', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 141);
    INSERT INTO user (login, password_hash, role, comission_id)
    VALUES ('okw10obkw10', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'OBWKW', 142);

-- Komitety
    INSERT INTO committee (full_name, short_name)
    VALUES ('Super Partia', 'SP');
    INSERT INTO committee (full_name, short_name)
    VALUES ('Ekstra Partia', 'EP');


INSERT INTO user (login, password_hash, role)
VALUES ('demo', '$2a$10$ebyC4Z5WtCXXc.HGDc1Yoe6CLFzcntFmfse6/pTj7CeDY5I05w16C', 'ADMIN');